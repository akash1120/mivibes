<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
<table width="330" border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td height="50" width="100%" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
        </tr>
        <tr>
        	<td><?= $content ?></td>
        </tr>
        <tr>
        	<td height="30" width="100%" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
        </tr>
        <tr>
            <td style="color: #939393; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; line-height: 24px; font-weight: 400; vertical-align: top; font-size: 14px; text-align: left;">
            Sincerely,<br />
            <strong><?= Yii::$app->params['siteName']?></strong><br />
            <a href="<?= Yii::$app->params['siteUrl']?>" style="text-decoration:none;"><span style="font-size:16px;color:#FF8C00;"><?= Yii::$app->params['siteUrl']?></span></a>
            </td>
        </tr>
    </tbody>
</table>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
