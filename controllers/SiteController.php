<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\helpers\Url;
use app\models\TermsAndConditions;
use app\models\CategoryFields;
use app\models\Category;
use app\models\CategorySearch;

class SiteController extends DefaultController
{
    public $layout = 'login';
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'index', 'flush-cache', 'test-email', 'fix-rec', 'logout','logout-admin', 'verify-payment', 'invoice-payment','admin','index-admin','user-list','index-hotelsettings'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'reset-password', 'forget-password', 'reset-password','admin','index-admin','user-list','index-hotelsettings'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'verify-payment' => ['post'],
                    /* 'verify-payment' => ['get'],*/
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $this->checkLogin();
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            if (!Yii::$app->user->isGuest) {
                return $this->redirect(['site/index-admin']);
            }
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            return $this->redirect(['site/index-hotelsettings']);
        }

        $this->layout = 'main';
        $searchModel = new CategorySearch();
        $searchModel->status = 1;
        $searchModel->parent_id = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'header_title' => 'Home',
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndexAdmin()
    {

        $this->checkAdminLogin();
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
           // $this->layout = 'main-admin';
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            return $this->redirect(['site/index-hotelsettings']);
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') != 3){
            return $this->redirect(['site/index']);
        }


        $this->layout = 'main-admin';
        $searchModel = new CategorySearch();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'header_title' => 'Home',
        ]);
    }


    public function actionIndexHotelsettings()
    {
       // die('dsd');
       // $this->checkAdminLogin();
        $session = Yii::$app->session;
       if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') != 3){
            return $this->redirect(['site/index']);
        }else if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
           return $this->redirect(['site/admin']);
    }

        $this->layout = 'main-hotel';
        $searchModel = new CategorySearch();
        $searchModel->status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-hotelsettings', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'header_title' => 'Home',
        ]);
    }


    /**
     * Flush Db Cache
     *
     * @return string
     */
    public function actionFlushCache()
    {
//		$this->checkLogin();
        Yii::$app->cache->flush();
        echo "Done";
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionFixRec($lastId = null)
    {
        $this->checkLogin();
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }

        $model = new LoginForm();

        if(isset($_POST['LoginForm']['type']) && $_POST['LoginForm']['type'] == 'login' ){

            $hotel = User::find()->where(['id' => $_POST['LoginForm']['username']])->asArray()->one();
            $_POST['LoginForm']['username'] = $hotel['username'];
        }
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionUserList($q = null, $id = null) {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select('id, name AS text')
                ->from('user')
                ->where(['like', 'name', $q])
                ->andwhere(['=', 'user_type', 0])
                ->limit(30);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
        }
        return $out;
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionAdmin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['site/index-admin']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login-admin', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionLogoutAdmin()
    {
        Yii::$app->user->logout();
        return $this->redirect(['site/admin']);
       // return Yii::$app->getResponse()->redirect('site/admin');

        //return $this->goHome();
    }

    /**
     * Displays forget page.
     *
     * @return Response|string
     */
    public function actionForgetPassword()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->sendEmail()) {
                echo "pass-sent";
                exit;
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->renderAjax('forget_password', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->resetPassword()) {
                Yii::$app->getSession()->addFlash('success', 'New password was saved.');
                return $this->redirect(['login']);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('reset_password', [
            'model' => $model,
        ]);
    }

    public function actionTestEmail()
    {
        if (Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
            ->setTo(["malick.naeem@gmail.com" => "Naeem"])
            ->setSubject('Email Test from Elite Booking')
            ->setTextBody("This is email body using Elite Booking")
            ->send()
        ) {
            echo "Sent";
        } else {
            echo "Error!";
        }
    }

    public function actionDefTestEmail()
    {
        if (Yii::$app->defmailer->compose()
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
            ->setTo(["malick.naeem@gmail.com" => "Naeem"])
            ->setSubject('Email Test from Elite Booking')
            ->setTextBody("This is email body using Elite Booking")
            ->send()
        ) {
            echo "Sent";
        } else {
            echo "Error!";
        }
    }

    /**
     * @param string $invoice_id
     * @return string|Response
     */
    public function actionInvoicePayment($invoice_id = "")
    {

        if ($invoice_id) {
            \Yii::$app->session->set('payInvoiceId', $invoice_id);
        } else {
            if (Yii::$app->session->get('payInvoiceId')) {
                $invoice_id = \Yii::$app->session->get('payInvoiceId');
            } else {
                Yii::$app->session->setFlash('error', "Invoice number does not exists");
                return $this->goHome();
            }
        }

        if (isset($invoice_id) && !empty($invoice_id)) {

            $errors = [];
            $invoice = Invoices::findOne($invoice_id);
            $result = "";
            $title = "Invoice Payment";
            $prod_name = "Invoice Payment";

            if ($invoice <> null) {

                // If invoice is already paid
                if ($invoice->status == 'paid') {
                    \Yii::$app->session->setFlash('error', "This invoice is already paid");
                    return $this->redirect('/');
                }
                $prod_name = $invoice->invoice_description;
            } else {
                Yii::$app->session->setFlash('error', "Invoice is invalid!");
                return $this->goHome();
            }

            $paytabs = new PaytabsHelper();
            $resp = $paytabs->create_pay_page($invoice);

            if (($resp <> null) && ($resp->response_code == '4012')) {

                //update user table
                $member = User::findOne($invoice->user_id);
                $member->p_id = $resp->p_id;
                $member->pay_page_url = $resp->payment_url;
                if (!$member->save()) {
                    return false;
                }
                //update invoice table
                $invoice->p_id = $resp->p_id;
                $invoice->pay_page_url = $resp->payment_url;
                if (!$invoice->save()) {
                    return false;
                }
            } else {
                $resp_err = $paytabs->responseCodes($resp->response_code);
                Yii::$app->getSession()->addFlash('error', Yii::t('app', $resp_err));
                return $this->redirect(Yii::$app->request->referrer ?: ['invoices/index']);
            }
            $this->redirect($resp->payment_url);
        }
    }

    /**
     * @param string $invoice_id
     * @return string|Response
     */

    public function actionVerifyPayment()
    {
        if (Yii::$app->session->get('payInvoiceId')) {
            $invoice_id = \Yii::$app->session->get('payInvoiceId');
            $invoice = Invoices::findOne($invoice_id);

        } else {
            Yii::$app->session->setFlash('error', "Invoice number does not exists");
            return $this->goHome();
        }
        if (\Yii::$app->request->post()) {

            $response = \Yii::$app->request->post();
            if (($response <> null) && (isset($response['payment_reference']))) {

                $paytabs = new PaytabsHelper();
                $resp_verify = $paytabs->verify_payment($response['payment_reference']);

                if (($resp_verify <> null) && ($resp_verify->response_code == 100)) {

                    //update user table
                    $member = User::findOne($invoice->user_id);
                    $member->payment_reference = $response['payment_reference'];
                    $member->pt_customer_email = $response['pt_customer_email'];
                    $member->pt_customer_password = $response['pt_customer_password'];
                    $member->pt_token = $response['pt_token'];
                    if (!$member->save()) {
                        return false;
                    }
                    //update invoice table
                    $invoice->payment_reference = $response['payment_reference'];
                    $invoice->pt_customer_email = $response['pt_customer_email'];
                    $invoice->pt_customer_password = $response['pt_customer_password'];
                    $invoice->pt_token = $response['pt_token'];
                    $invoice->status = 'paid';
                    if (!$invoice->save()) {
                        return false;
                    }

                    $model = new Payments();
                    $model->payment_ref = $response['payment_reference'];
                    $model->invoice_id = $invoice_id;
                    $model->pt_invoice_id = $resp_verify->pt_invoice_id;
                    $model->trax_ref = $resp_verify->reference_no;
                    $model->payment_method_id = 2;
                    $model->frequency = ($invoice->billing_cycle <> null)? $invoice->billing_cycle : '1';
                    $model->currency = $resp_verify->currency;
                    $model->transaction_id = $resp_verify->transaction_id;
                    $model->status = 'paid';
                    $model->user_id = $invoice->user_id;
                    $model->amount = $resp_verify->amount;
                    $model->response_code = $resp_verify->response_code;
                    $model->date = date('Y-m-d H:i:s');
                    $model->note = $resp_verify->result;
                    if (!$model->save()) {
                         var_dump($model->errors);
                        return false;
                    }

                    //update invoice status
                    $previous_payments= Payments::find()->where(['invoice_id' => $invoice_id])->asArray()->all();
                    $paid_amount = 0;
                    if(isset($previous_payments) && $previous_payments <> null){
                        foreach ($previous_payments as $key => $payment){
                            $paid_amount = $paid_amount + $payment['amount'];
                        }
                    }


                    if($invoice->total > ($paid_amount)){
                        $invoice->status = 'partial';
                    }else{
                        $invoice->status = 'paid';
                    }

                   // $invoice->status = 'paid';
                    if (!$invoice->save()) {
                        return false;
                    }

                    //update contract status
                    $current_date = date('y-m-d');
                    $contract = Contracts::findOne($invoice->contract_id);;
                    $contract_end_date = date('Y-m-d', strtotime($contract->end_date));
                    if ($contract->current_expiry <> null) {
                        $contract_end_date = date('Y-m-d', strtotime($contract->current_expiry));
                    }
                    if ((strtotime($current_date) >= strtotime($contract->start_date)) && (strtotime($current_date) <= strtotime($contract_end_date))) {
                        $contract->status = 'active';
                        if (!$contract->save()) {
                            return false;
                        }
                    }

                    //receipts entry
                    $model_reciepts = new Reciepts();
                    $model_reciepts->payment_ref = $response['payment_reference'];
                    $model_reciepts->invoice_id = $invoice_id;
                    $model_reciepts->pt_invoice_id = $resp_verify->pt_invoice_id;
                    $model_reciepts->trax_ref = $resp_verify->reference_no;
                    $model_reciepts->payment_method_id = 2;
                    $model_reciepts->frequency = ($invoice->billing_cycle <> null)? $invoice->billing_cycle : '1';
                    $model_reciepts->currency = $resp_verify->currency;
                    $model_reciepts->transaction_id = $resp_verify->transaction_id;
                    $model_reciepts->status = 'paid';
                    $model_reciepts->user_id = $invoice->user_id;
                    $model_reciepts->amount = $resp_verify->amount;
                    $model_reciepts->response_code = $resp_verify->response_code;
                    $model_reciepts->date = date('Y-m-d H:i:s');
                    $model_reciepts->note = $resp_verify->result;
                    $model_reciepts->payment_id = $model->id;
                    if (!$model_reciepts->save()) {
                        return false;
                    }


                   /* $contract = Contracts::findOne($invoice->contract_id);;
                    $contract->status = 'active';
                    if (!$contract->save()) {
                        return false;
                    }*/
                    //send email
                    $terms = TermsAndConditions::find()->where(['id' => 1])->asArray()->one();
                    $model->sendReceiptEmail($model, $terms);

                }else{
                    $contract = Contracts::findOne($invoice->contract_id);;
                    if (!empty($contract)){
                        $contract->status = 'declined';
                        if (!$contract->save()) {
                            return false;
                        }
                    }
                    Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Payment has been declined! Please check your details!'));
                    return $this->redirect(Yii::$app->request->referrer ?: ['invoices/index']);
                }
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Payment has been done successfully'));
                return $this->redirect(Yii::$app->request->referrer ?: ['invoices/index']);
            }
        }

        // $this->goHome();
    }

    public function actionPaymentComplete()
    {

        echo "<pre>";
        print_r($_POST);
        die;
    }



}