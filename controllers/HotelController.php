<?php

namespace app\controllers;

use app\models\Area;
use app\models\City;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\UserSearch;
use yii\web\NotFoundHttpException;
use app\models\CategorySearch;
use app\models\DealSearch;

/**
 * Hotel Controller implements the CRUD actions for User model.
 */
class HotelController extends UserController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','view','update','delete','getareasofcity'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
				'denyCallback' => function ($rule, $action) {
					if(Yii::$app->request->isAjax){
						throw new \yii\web\HttpException(401, Yii::t('app','Please login.'));
					}else{
						return $this->redirect(['site/login']);
					}
				}
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		$this->checkAdmin();
        $this->layout = 'main-admin';
        $searchModel = new UserSearch();
		$searchModel->user_type = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->checkAdmin();
            $this->layout = 'main-admin';
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            $this->checkLogin();
            $this->layout = 'main-hotel';
        }
        //$this->layout = 'main-admin';
        $model = new User();
		$model->user_type = 0;
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
				return $this->redirect(['index']);
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->checkAdmin();
            $this->layout = 'main-admin';
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            $this->checkLogin();
            $this->layout = 'main-hotel';
        }

		/*$this->checkAdmin();
        $this->layout = 'main-admin';*/
        $model = $this->findModel($id);
		$model->oldimage=$model->image;
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));

                if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
                    return $this->redirect(['index']);
                }else{
                    return $this->redirect(['hotel/update?id='.Yii::$app->user->identity->id]);
                }



			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionView($id)
    {

        $this->layout = 'main-admin';

        $searchModel = new DealSearch();
        $searchModel->hotel_id= $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);




        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetareasofcity()
    {
        if (Yii::$app->request->get()) {
            $city_id = Yii::$app->request->get('id');
            $area_id= '';

            if(Yii::$app->request->get('areaid')){
                $area_id = Yii::$app->request->get('areaid');
            }
            $package_data = City::findOne($city_id);
            $result = Area::find()->where(['city_id' => $city_id])->asArray()->all();
            $html = ' <option selected value="">Select Area</option>';
            $html2 = '';
            foreach ($result as $key => $res) {
                if(($area_id <> null) && ($res['id']== $area_id)){
                    $html .= ' <option selected value="'.$res['id'].'">'.$res['name'].'</option>';
                }
                else{
                    $html .= ' <option value="'.$res['id'].'">'.$res['name'].'</option>';
                }

            }
            echo json_encode(['options' =>$html]);
            exit;
        }
    }
}
