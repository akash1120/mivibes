<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\UserSearch;
use yii\web\NotFoundHttpException;

/**
 * StaffController implements the CRUD actions for User model.
 */
class StaffController extends UserController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','create','view','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
				'denyCallback' => function ($rule, $action) {
					if(Yii::$app->request->isAjax){
						throw new \yii\web\HttpException(401, Yii::t('app','Please login.'));
					}else{
						return $this->redirect(['site/login']);
					}
				}
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		$this->checkAdmin();
        $searchModel = new UserSearch();
		$searchModel->user_type = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$this->checkAdmin();
        $model = new User();
		$model->user_type = 1;
		$model->is_licensed = 1;
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
				return $this->redirect(['index']);
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$this->checkAdmin();
        $model = $this->findModel($id);
		$model->oldimage=$model->image;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
            	Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
            	return $this->redirect(['index']);
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
			}
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
