<?php

namespace app\controllers;

use Yii;
use app\models\Deal;
use app\models\DealSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DealController implements the CRUD actions for Deal model.
 */
class DealController extends DefaultController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Deal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->checkLogin();
        $searchModel = new DealSearch();

        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->layout = 'main-admin';
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            $this->layout = 'main-hotel';
            $searchModel->hotel_id = Yii::$app->user->identity->id;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }

       // $this->layout = 'main-admin';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Deal models.
     * @return mixed
     */
    public function actionFilterResults()
    {


        if ($type = Yii::$app->request->get('type')) {


            $searchModel = new DealSearch();
            if($type == 'hot'){
                $searchModel->hot_deal = 1;
            }else{
                $searchModel->category_id = Yii::$app->request->get('type');
            }
            $searchModel->pageSize = 50;
           // $searchModel->category_id = Yii::$app->request->get('type');
            $dataProvider = $searchModel->searchfilter(Yii::$app->request->queryParams);
            $datafilters = Yii::$app->request->queryParams;
            if($type == 'hot'){
                $datafilters['DealSearch']['hot_deal'] = Yii::$app->request->get('type');
            }else{
                $datafilters['DealSearch']['category_id'] = Yii::$app->request->get('type');
            }


            return $this->render('filterresults', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'datafilters' => $datafilters['DealSearch'],
                'datafilters_all' => $datafilters['DealSearch'],
            ]);
        } else if (Yii::$app->request->get('remove_filter')) {
            $removed = Yii::$app->request->get('remove_filter');
            $new_filters = Yii::$app->request->queryParams;
            if($new_filters['category_id'] == 3){
                return $this->redirect(['deal/filter?id=3']);


            }
            unset($new_filters[$removed]);
            unset($new_filters['id']);
            unset($new_filters['remove_filter']);
            $filter['DealSearch'] = $new_filters;
            Yii::$app->request->queryParams = $filter;
            $searchModel = new DealSearch();
            $dataProvider = $searchModel->searchfilter(Yii::$app->request->queryParams);
            $datafilters = Yii::$app->request->queryParams;


            return $this->render('filterresults', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'datafilters' => $datafilters['DealSearch'],
            ]);
        } else {
            $searchModel = new DealSearch();
            $dataProvider = $searchModel->searchfilter(Yii::$app->request->queryParams);
            $datafilters = Yii::$app->request->queryParams;

            return $this->render('filterresults', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'datafilters' => $datafilters['DealSearch'],
            ]);
        }
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionFilter($id, $type=null)
    {
       // $this->checkAdmin();
        $searchModel = new DealSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('filter', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category_id' => $id,
            'type' => $type,
        ]);
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionSearchall()
    {
        //$this->checkAdmin();
        $searchModel = new DealSearch();
        $dataProvider = $searchModel->searchfilter(Yii::$app->request->queryParams);


        return $this->render('search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Deal models.
     * @return mixed
     */
    public function actionSearchResults()
    {
        $searchModel = new DealSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $datafilters = Yii::$app->request->queryParams;

        return $this->render('searchresults', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'datafilters' => $datafilters['DealSearch'],
        ]);
    }


    /**
     * Displays a single Deal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->layout = 'main-admin';
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            $this->layout = 'main-hotel';
        }



       // $this->layout = 'main-admin';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Deal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionViewdeal($id)
    {
        return $this->render('viewdeal', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Deal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Deal();
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->layout = 'main-admin';
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            $this->layout = 'main-hotel';
        }

        //$this->layout = 'main-admin';
        $type = Yii::$app->request->get('type');
        $model->category_id = $type;

        if(isset($model->category->parent_id) && $model->category->parent_id == 3){
            $model->leisure_deal = 1;
        }else{
            $model->leisure_deal = 0;
        }

        if(isset($model->hot_deal) && $model->hot_deal <> null){
            $model->hot_deal = 1;
        }else{
            $model->hot_deal = 0;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Deal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->layout = 'main-admin';
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            $this->layout = 'main-hotel';
        }
      //  $this->layout = 'main-admin';
        $model = $this->findModel($id);
        $model->oldimage = $model->image;
        $model->oldimage2 = $model->image2;
        $model->oldimage3 = $model->image3;
        if(isset($model->category->parent_id) && $model->category->parent_id == 3){
            $model->leisure_deal = 1;
        }else{
            $model->leisure_deal = 0;
        }



        if ($model->load(Yii::$app->request->post())) {
            if(isset($_POST['Deal']['hot_check'])){
                $model->hot_deal = 1;
            }else{
                $model->hot_deal= 0;
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Deal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $session = Yii::$app->session;
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->layout = 'main-admin';
        }else if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){
            $this->layout = 'main-hotel';
        }
        //$this->layout = 'main-admin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Deal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
