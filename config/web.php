<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [

        'gridview' => [
            'class' => '\kartik\grid\Module'

        ],
    ],
    'components' => [
        /*'assetManager'=>[
			'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'assets',
			'baseUrl'=>'/assets'
		],*/
		'helperFunction' => [
            'class' => 'app\helpers\HelperFunction',
        ],
		'formatter' => [
			'class' => 'yii\i18n\Formatter',
			'dateFormat' => 'php:D, M j, Y',
			'datetimeFormat' => 'php:D, j M, Y / h:i a',
			'timeFormat' => 'php:h:i a',
			'timeZone' => 'Asia/Dubai',
		],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'SK1McSTWQOIBQ5S1Wbhjq-W82XiJvNIEz',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'defmailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
			/*'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'mail.booking.elitecaptains.ae',
				'username' => 'no-reply@booking.elitecaptains.ae',
				'password' => 'Boating@987',
				'port' => '465',
				'encryption' => 'ssl',
            ],*/
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'smtp.gmail.com',
                /*'username' => 'no-reply@booking.elitecaptains.ae',
                'password' => 'Boating@987',*/
				'port' => '587',
				'encryption' => 'ssl',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
					'except' => ['yii\web\HttpException:404','yii\web\HttpException:403','yii\web\HttpException:405','yii\web\HttpException:400','yii\db\Exception','Swift_TransportException','yii\debug\Module::checkAccess'],
                ],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['yii\web\HttpException:404'],
					'logFile' => '@runtime/logs/404.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['yii\web\HttpException:403'],
					'logFile' => '@runtime/logs/403.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['Swift_TransportException'],
					'logFile' => '@runtime/logs/swift_mailer.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['yii\debug\Module::checkAccess'],
					'logFile' => '@runtime/logs/debug_access.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['yii\web\HttpException:405'],
					'logFile' => '@runtime/logs/method_not_allowed405.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['yii\web\HttpException:400'],
					'logFile' => '@runtime/logs/noverifydata.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error'],
					'categories' => ['yii\db\Exception'],
					'logFile' => '@runtime/logs/db_errors.log',
				],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'http://local.mivibes/admin' => 'site/admin',
                'http://local.mivibes/login' => 'site/login',
            ],
        ],
        'pdf' => [
            'class' => \kartik\mpdf\Pdf::classname(),
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            // refer settings section for all chomidconfiguration options
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
