<?php
namespace app\assetfiles;

use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/Ionicons/css/ionicons.min.css',
		'css/AdminLTE.min.css',
		'css/skin-black-light.min.css',
		'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
    ];
    public $js = [
        'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'plugins/fastclick/lib/fastclick.js',
        'js/adminlte.min.js',
    ];
}
