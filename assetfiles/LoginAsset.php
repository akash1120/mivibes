<?php
namespace app\assetfiles;

use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.css',
        'css/jquery-ui.css',
        'css/magic-check.css',
        'css/magic-check.min.css',
        'css/magic-check.min.css',
        'css/stylesheet.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
		'yii\bootstrap\BootstrapThemeAsset',
		'app\assetfiles\FontAwesomeAsset',
		'app\assetfiles\ThemeAsset',
    ];
}
