<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Change Password form
 */
class ChangePasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $confirm_password;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_password', 'new_password', 'confirm_password'], 'required'],
			['confirm_password','compare', 'compareAttribute'=>'new_password'],
            ['old_password', 'validatePassword'],
            ['new_password', 'string', 'min' => 6],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
			if (!$user || !$user->validatePassword($this->old_password)) {
                $this->addError($attribute, Yii::t('app','Incorrect old password.'));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'old_password' => Yii::t('app', 'Old Password'),
            'new_password' => Yii::t('app', 'New Password'),
            'confirm_password' => Yii::t('app', 'Confirm Password'),
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function save()
    {
        if ($this->validate()) {
            $user = $this->getUser();
			$user->password=$this->new_password;
			$user->save();
			return true;
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername(Yii::$app->user->identity->email);
        }

        return $this->_user;
    }
}
