<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "deal".
 *
 * @property int $id
 * @property string $title
 * @property int $hotel_id
 * @property int $category_id
 * @property int $discount
 * @property string $description
 * @property string $terms
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $facebook
 * @property string $instagram
 * @property string $rest_name
 * @property string $cuisine
 * @property int $status
 */
class Deal extends \yii\db\ActiveRecord
{
    public $oldimage,$oldimage2,$oldimage3,$imagefile,$imagefile2,$imagefile3,$city_id,$area_id,$cuisine,$hot_check,$leisure_deal,$subcategory;
    public $allowedImageSize = 1024000;
    public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
    public $additionalFields = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'discount', 'status'], 'integer'],
            [['description', 'terms'], 'string'],
            [['additionalFields','terms','hot_deal','hot_check','discount_description','hotel_id','cities','start_date','end_date','map_link','sub_category'], 'safe'],
            [['email'], 'email'],
            [['image','image2','image3'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes)],
            [['title', 'phone', 'email', 'website', 'facebook', 'instagram', 'rest_name', 'cuisine'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'hotel_id' => 'Hotel ID',
            'category_id' => 'Category ID',
            'discount' => 'Discount',
            'description' => 'Description',
            'terms' => 'Terms',
            'phone' => 'Phone',
            'email' => 'Email',
            'website' => 'Website',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'rest_name' => 'Rest Name',
            'cuisine' => 'Cuisine',
            'status' => 'Status',
        ];
    }
    /**
     * return html status
     */
    public function getTxtStatus()
    {
        return $this->status==1 ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Blocked</span>';
    }

    /**
     * @return image path
     */
    public function getImageSrc()
    {

        $image=Yii::$app->params['default_avatar'];
        if($this->image!=null && file_exists(Yii::$app->params['avatar_abs_path'].$this->image)){
            $image=Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $this->image, 128, 128);
        }
        return $image;
    }
    /**
     * @return image 2 path
     */
    public function getImageSrc2()
    {

        $image=Yii::$app->params['default_avatar'];
        if($this->image2!=null && file_exists(Yii::$app->params['avatar_abs_path'].$this->image2)){
            $image=Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $this->image2, 128, 128);
        }
        return $image;
    }

    /**
     * @return image 3 path
     */
    public function getImageSrc3()
    {

        $image=Yii::$app->params['default_avatar'];
        if($this->image3!=null && file_exists(Yii::$app->params['avatar_abs_path'].$this->image3)){
            $image=Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $this->image3, 128, 128);
        }
        return $image;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        //Uploading image
        if(UploadedFile::getInstance($this, 'image')){
            $this->imagefile = UploadedFile::getInstance($this, 'image');
            // if no file was uploaded abort the upload
            if (!empty($this->imagefile) && file_exists($this->imagefile->tempName)) {
                $pInfo=pathinfo($this->imagefile->name);
                $ext = $pInfo['extension'];
                if (in_array($ext,$this->allowedImageTypes)) {
                    // Check to see if any PHP files are trying to be uploaded
                    $content = file_get_contents($this->imagefile->tempName);
                    if (preg_match('/\<\?php/i', $content)) {
                        $this->addError('image', Yii::t('app', 'Invalid file provided!'));
                        return false;
                    }else{
                        if (filesize($this->imagefile->tempName) <= $this->allowedImageSize) {
                            // generate a unique file name
                            $this->image = Yii::$app->controller->generateName().".{$ext}";
                        }else{
                            $this->addError('image', Yii::t('app', 'File size is too big!'));
                            return false;
                        }
                    }
                }else{
                    $this->addError('image', Yii::t('app', 'Invalid file provided!'));
                    return false;
                }
            }
        }
        if($this->image == null && $this->oldimage!= null){
            $this->image=$this->oldimage;
        }


        //Uploading image 2
        if(UploadedFile::getInstance($this, 'image2')){
            $this->imagefile2 = UploadedFile::getInstance($this, 'image2');
            // if no file was uploaded abort the upload
            if (!empty($this->imagefile2) && file_exists($this->imagefile2->tempName)) {
                $pInfo=pathinfo($this->imagefile2->name);
                $ext = $pInfo['extension'];
                if (in_array($ext,$this->allowedImageTypes)) {
                    // Check to see if any PHP files are trying to be uploaded
                    $content = file_get_contents($this->imagefile2->tempName);
                    if (preg_match('/\<\?php/i', $content)) {
                        $this->addError('image2', Yii::t('app', 'Invalid file provided!'));
                        return false;
                    }else{
                        if (filesize($this->imagefile2->tempName) <= $this->allowedImageSize) {
                            // generate a unique file name
                            $this->image2 = Yii::$app->controller->generateName().".{$ext}";
                        }else{
                            $this->addError('image2', Yii::t('app', 'File size is too big!'));
                            return false;
                        }
                    }
                }else{
                    $this->addError('image2', Yii::t('app', 'Invalid file provided!'));
                    return false;
                }
            }
        }
        if($this->image2 == null && $this->oldimage2!=null){
            $this->image2 = $this->oldimage2;
        }



        //Uploading image 3
        if(UploadedFile::getInstance($this, 'image3')){
            $this->imagefile3 = UploadedFile::getInstance($this, 'image3');
            // if no file was uploaded abort the upload
            if (!empty($this->imagefile3) && file_exists($this->imagefile3->tempName)) {
                $pInfo=pathinfo($this->imagefile3->name);
                $ext = $pInfo['extension'];
                if (in_array($ext,$this->allowedImageTypes)) {
                    // Check to see if any PHP files are trying to be uploaded
                    $content = file_get_contents($this->imagefile3->tempName);
                    if (preg_match('/\<\?php/i', $content)) {
                        $this->addError('image3', Yii::t('app', 'Invalid file provided!'));
                        return false;
                    }else{
                        if (filesize($this->imagefile3->tempName) <= $this->allowedImageSize) {
                            // generate a unique file name
                            $this->image3 = Yii::$app->controller->generateName().".{$ext}";
                        }else{
                            $this->addError('image3', Yii::t('app', 'File size is too big!'));
                            return false;
                        }
                    }
                }else{
                    $this->addError('image3', Yii::t('app', 'Invalid file provided!'));
                    return false;
                }
            }
        }
        if($this->image3 == null && $this->oldimage3 != null){
            $this->image3 = $this->oldimage3;
        }




        return parent::beforeValidate();
    }
    public function beforeSave($insert)
    {
        if(isset($this->id) && $this->id != null){

        }else{
            $this->created_date = date('Y-m-d');
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->imagefile!== null && $this->image!=null) {
            if($this->oldimage!=null && $this->image!=$this->oldimage && file_exists(Yii::$app->params['avatar_abs_path'].$this->oldimage)){
                unlink(Yii::$app->params['avatar_abs_path'].$this->oldimage);
            }
            $this->imagefile->saveAs(Yii::$app->params['avatar_abs_path'].$this->image);
        }
        if ($this->imagefile2!== null && $this->image2!=null) {
            if($this->oldimage2!=null && $this->image2!=$this->oldimage2 && file_exists(Yii::$app->params['avatar_abs_path'].$this->oldimage2)){
                unlink(Yii::$app->params['avatar_abs_path'].$this->oldimage2);
            }
            $this->imagefile2->saveAs(Yii::$app->params['avatar_abs_path'].$this->image2);
        }
        if ($this->imagefile3!== null && $this->image3!=null) {
            if($this->oldimage3!=null && $this->image3!=$this->oldimage3 && file_exists(Yii::$app->params['avatar_abs_path'].$this->oldimage3)){
                unlink(Yii::$app->params['avatar_abs_path'].$this->oldimage3);
            }
            $this->imagefile3->saveAs(Yii::$app->params['avatar_abs_path'].$this->image3);
        }


        DealFields::deleteAll(['deal_id' => $this->id]);

        // Save Additional request fileds
        foreach ($this->additionalFields as $field) {
            $requestTypes = new DealFields();
            $requestTypes->attributes = $field;
            $requestTypes->deal_id = $this->id;
            $requestTypes->save();
        }

        $current_date = date('Y-m-d');

            if($this->category->parent_id == 3){

                if($this->start_date == $current_date){

                    if($this->status == 1) {
                        $content = array(
                            "en" => 'Mivibes updated Deal ' . $this->title
                        );

                        $fields = array(
                            'app_id' => "e1f51b59-270a-4e39-aa36-db5d790478de",
                            'included_segments' => array('All'),
                            'data' => array("foo" => "bar"),
                            'large_icon' => "ic_launcher_round.png",
                            'contents' => $content
                        );

                        $fields = json_encode($fields);
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                            'Authorization: Basic OWQ0NmI3NTQtNDFkYS00MjQ1LWI1MWEtNDE0MWNhNDg5NTFl'));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                        curl_setopt($ch, CURLOPT_HEADER, FALSE);
                        curl_setopt($ch, CURLOPT_POST, TRUE);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                        $response = curl_exec($ch);
                        curl_close($ch);
                    }
                }

            }else {

                if($this->status == 1) {
                    $content = array(
                        "en" => 'Mivibes updated Deal ' . $this->title
                    );

                    $fields = array(
                        'app_id' => "e1f51b59-270a-4e39-aa36-db5d790478de",
                        'included_segments' => array('All'),
                        'data' => array("foo" => "bar"),
                        'large_icon' => "ic_launcher_round.png",
                        'contents' => $content
                    );

                    $fields = json_encode($fields);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                        'Authorization: Basic OWQ0NmI3NTQtNDFkYS00MjQ1LWI1MWEtNDE0MWNhNDg5NTFl'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                    $response = curl_exec($ch);
                    curl_close($ch);
                }
            }







        parent::afterSave($insert, $changedAttributes);




    }
    /**
     * return Hotel
     */
    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'hotel_id']);
    }
    /**
     * return Hotel
     */
    public function getHotel(){
        return $this->hasOne(User::className(),['id' => 'hotel_id']);
    }
    /**
     * return Hotel
     */
    public function getCategory(){
        return $this->hasOne(Category::className(),['id' => 'category_id']);
    }
    public function getFieldByTypeFiled($field_type_id)
    {
        return DealFields::find()->where([
            'deal_id' => $this->id,
            'category_filed_id' => $field_type_id
        ])->one();
    }
    /**
     * return Hotel
     */
    public function getDealFields(){
        return $this->hasMany(DealFields::className(),['deal_id' => 'id']);
    }



}
