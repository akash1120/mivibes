<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required', 'message'=>'Please enter your email address'],
            ['username', 'email', 'message'=>'Please enter your valid email address'],
            ['username', 'exist',
                'targetClass' => '\app\models\User',
				'targetAttribute' => 'email',
                'filter' => ['trashed'=>0],
                'message' => Yii::t('app','There is no user with such email.')
            ],
            ['username', 'checkBlock'],
        ];
    }
	
    /**
     * checks the email, if it contains @, add error
     */
	public function checkBlock($attribute, $params)
	{
		$user = User::findOne(['email' => $this->username,]);
		if($user->status==User::STATUS_BLOCKED){
			$this->addError($attribute, Yii::t('app','Sorry, your account is blocked, Please contact the administration'));
			return false;
		}
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Enter your email address'),
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
		if($this->validate()){
			/* @var $user User */
			$user = User::findOne([
				'status' => User::STATUS_ACTIVE,
				'email' => $this->username,
			]);
			if ($user) {
				if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
					$user->generatePasswordResetToken();
				}
	
				if ($user->update()) {
					
					return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
						->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']
						])
						->setTo($this->username)
						->setSubject(Yii::t('app','Password reset for') . ' ' . Yii::$app->params['siteName'])
						->send();
				}
			}
		}else{
        	return false;
		}
    }
}
