<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deal_fields".
 *
 * @property int $id
 * @property int $category_filed_id
 * @property int $deal_id
 * @property string $value
 */
class DealFields extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deal_fields';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_filed_id', 'deal_id', 'value'], 'required'],
            [['category_filed_id', 'deal_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['category_filed_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryFields::className(), 'targetAttribute' => ['category_filed_id' => 'id']],
            [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_filed_id' => 'Category Filed ID',
            'deal_id' => 'Deal ID',
            'value' => 'Value',
        ];
    }
}
