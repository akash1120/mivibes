<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property int $id
 * @property string $title
 * @property string $keyword
 */
class Service extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'keyword'], 'required'],
            [['title'], 'string', 'max' => 32],
            [['keyword'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'keyword' => Yii::t('app', 'Keyword'),
		];
    }
}
