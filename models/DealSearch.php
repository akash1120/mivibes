<?php

namespace app\models;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Deal;

/**
 * DealSearch represents the model behind the search form of `app\models\Deal`.
 */
class DealSearch extends Deal
{
    public $pageSize;
    public $city_id,$area_id,$cuisine;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'hotel_id', 'category_id', 'discount', 'status','city_id','area_id','hot_deal','pageSize'], 'integer'],
            [['title', 'description', 'terms', 'phone', 'email', 'website', 'facebook', 'instagram', 'rest_name', 'cuisine','cities','subcategory','sub_category'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
            $this->load($params);
            $query = Deal::find();
          //  $query = Deal::find()->joinWith('dealFields');
        }else {
            $query = Deal::find()->joinWith('user')->joinWith('dealFields');
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if(isset($this->rest_name) &&  $this->rest_name <> null) {

                $query->join('LEFT JOIN', 'deal_fields', 'deal_fields.deal_id = deal.id');

                $query->andFilterWhere(['like', 'deal_fields.value', $this->rest_name]);
        }
      /*  echo "<pre>";
        print_r($this);
        die;*/

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hotel_id' => $this->hotel_id,
            'category_id' => $this->category_id,
            'discount' => $this->discount,
            'status' => $this->status,
            'hot_deal' => $this->hot_deal,
        ]);

        $query
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'terms', $this->terms])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'facebook', $this->facebook])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
           /* ->andFilterWhere(['like', 'rest_name', $this->rest_name])
            ->andFilterWhere(['like', 'cuisine', $this->cuisine])*/
        ;
        $query->andFilterWhere(['or',
            ['like', 'value', $this->title],
            ['like', 'value', $this->title],
            ['like', 'title', $this->title],
        ]);

        return $dataProvider;
    }
    public function searchfilter($params)
    {
        //$query = Deal::find();
        $query = Deal::find()->joinWith('user')->joinWith('dealFields');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(isset($this->subcategory) &&  $this->subcategory <> null) {
            $query->andFilterWhere([
                'category_id' => $this->subcategory,

            ]);


        }else if($this->category_id != 3){

            $query->andFilterWhere([
                'id' => $this->id,
                'hotel_id' => $this->hotel_id,
                'category_id' => $this->category_id,
                'discount' => $this->discount,
                'status' => $this->status,
            ]);
            // hide expired hot deals
            if($this->category_id == 2){
                $current_date = date('Y-m-d');
                $query->andFilterWhere(['>=', 'end_date', $current_date]);
            }
        }else if($this->category_id == 3){
            $query->andWhere(['not in', 'category_id',[1,2,3]]);
        }
        // grid filtering conditions
        if(isset($this->city_id) &&  $this->city_id <> null) {
            $query->andFilterWhere([
                'city_id' => $this->city_id,
            ]);
        }
        if(isset($this->hot_deal) &&  $this->hot_deal <> null) {
            $query->andFilterWhere([
                'hot_deal' => $this->hot_deal,
            ]);
        }
        if(isset($this->title) &&  $this->title <> null) {
            $query->andFilterWhere([
                /*'title' => $this->title,*/
                'or',
                ['like', 'deal.title', $this->title],
            ]);
        }
        if(isset($this->cities) &&  $this->cities <> null) {
            $query->andFilterWhere([
                'cities' => $this->cities,
            ]);
        }
        if(isset($this->cuisine) &&  $this->cuisine <> null) {
            $query->andFilterWhere(['and',
                ['like', 'value', $this->cuisine]
            ]);
        }
        if(isset($this->area_id) &&  $this->area_id <> null) {
            $query->andFilterWhere([
                'area_id' => $this->area_id,
            ]);
        }
        if(isset($this->sub_category) &&  $this->sub_category <> null) {
            $query->andFilterWhere([
                'sub_category' => $this->sub_category,
            ]);
        }

        return $dataProvider;
    }
}
