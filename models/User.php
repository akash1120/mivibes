<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

class User extends ActiveRecord implements IdentityInterface
{
	public $oldPackageId,$new_password,$type,$admin_type;
	public $sub_user_id,$sub_city_id,$sub_username,$sub_new_password,$sub_email,$sub_mobile,$sub_is_licensed,$sub_image,$sub_imageSrc,$sub_notice,$sub_notice_class;
	
	public $oldimage,$imagefile,$oldsubimage,$subimagefile;
	public $allowedImageSize = 512000;
	public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
	
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 0;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
	
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
                    return date("Y-m-d H:i:s"); 
                },
			],
			'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
		];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['user_type','city_id','username','email','address','name'], 'required'],
			[['user_type','city_id','area_id','status'], 'integer'],
			[['username', 'email', 'mobile','address','name'], 'string'],
			[['email'], 'email'],
			[['new_password','user_password','admin_password'], 'string', 'min' => 6],
			['email','unique','filter' => ['trashed' => 0],'message'=>Yii::t('app','Email "{value}" is already registered')],
			['username','unique','filter' => ['trashed' => 0],'message'=>Yii::t('app','Username "{value}" is already registered')],
            ['user_type', 'default', 'value' => 0],
            ['status', 'default', 'value' => self::STATUS_BLOCKED],
            ['status', 'in', 'range' => [self::STATUS_BLOCKED,self::STATUS_ACTIVE]],
            [['image','sub_image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes)],
            [['user_password','admin_password','admin_password_hash'], 'safe'],
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Mobile'),
            'new_password' => Yii::t('app', 'Password'),
            'txtStatus' => Yii::t('app', 'Status'),
        ];
    }
	
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE, 'trashed' => 0]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		if($token!=null){
			return static::findOne(['auth_key' => $token, 'status' => self::STATUS_ACTIVE, 'trashed' => 0]);
		}else{
			return null;
		}
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'trashed' => 0]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
			'trashed' => 0
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
		//return Yii::$app->security->validatePassword($password, $this->password_hash);
        if(Yii::$app->security->validatePassword($password, $this->password_hash)){
            $session = Yii::$app->session;
            $session->open();
            $session->set('user_type_admin', '1');
            $session->close();
            return true;
        }else if( Yii::$app->security->validatePassword($password, $this->admin_password_hash)){
            $session = Yii::$app->session;
            $session->open();
            $session->set('user_type_admin', '3');
            $session->close();
            return true;
        }else{
            return false;
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
	
    /**
     * return html status
     */
	public function getTxtStatus()
	{
		return $this->status==1 ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Blocked</span>';
	}
	
    /**
     * return html is licensed
     */
	public function getTxtLicensed()
	{
		return $this->is_licensed==1 ? '<span class="label label-success">Yes</span>' : '<span class="label label-warning">No</span>';
	}

    /**
     * Get city row
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
    /**
     * Get city row
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    /**
     * return login count
     */
	public function getLoginCount()
	{
		return UserLoginHistory::find()->where(['user_id'=>$this->id])->count('id');
	}
	
    /**
     * return last login date
     */
	public function getLastloginDate()
	{
		$result=UserLoginHistory::find()->where(['user_id'=>$this->id, 'login'=>1])->orderBy('id desc')->asArray()->limit(1)->offset(1)->one();
		if($result!=null){
			return $result->created_at;
		}else{
			return null;
		}
	}
	
    /**
     * @return image path
     */
	public function getImageSrc()
	{
		$image=Yii::$app->params['default_avatar'];
		if($this->image!=null && file_exists(Yii::$app->params['avatar_abs_path'].$this->image)){
			$image=Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $this->image, 128, 128);
		}
		return $image;
	}

    /**
	 * Logs activity of user
     * @return boolean
     */
	public function LogActivity($source,$source_id,$action_type,$descp=null)
	{
		$model=new UserActivity;
		$model->source=$source;
		$model->source_id=$source_id;
		$model->action_type=$action_type;
		if($model->save()){
			return true;
		}else{
			return false;
		}
	}

    /**
     * @inheritdoc
     */
	public function beforeValidate()
	{	
		//Uploading image
		if(UploadedFile::getInstance($this, 'image')){
			$this->imagefile = UploadedFile::getInstance($this, 'image');
			// if no file was uploaded abort the upload
			if (!empty($this->imagefile) && file_exists($this->imagefile->tempName)) {
				$pInfo=pathinfo($this->imagefile->name);
				$ext = $pInfo['extension'];
				if (in_array($ext,$this->allowedImageTypes)) {
					// Check to see if any PHP files are trying to be uploaded
					$content = file_get_contents($this->imagefile->tempName);
					if (preg_match('/\<\?php/i', $content)) {
						$this->addError('image', Yii::t('app', 'Invalid file provided!'));
						return false;
					}else{
						if (filesize($this->imagefile->tempName) <= $this->allowedImageSize) {
							// generate a unique file name
							$this->image = Yii::$app->controller->generateName().".{$ext}";
						}else{
							$this->addError('image', Yii::t('app', 'File size is too big!'));
							return false;
						}
					}
				}else{
					$this->addError('image', Yii::t('app', 'Invalid file provided!'));
					return false;
				}
			}
		}
		if($this->image==null && $this->oldimage!=null){
			$this->image=$this->oldimage;
		}
		return parent::beforeValidate();
	}
	
    /**
     * @inheritdoc
     */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if($this->new_password!=null){
				$this->password=$this->new_password;
				$this->user_password=$this->new_password;
			}
            if($this->admin_password!=null){
                $this->admin_password=$this->admin_password;
                $this->admin_password_hash = Yii::$app->security->generatePasswordHash($this->admin_password);
            }
			if($this->auth_key==null){
				$this->generateAuthKey();
			}
			return true;
		} else {
			return false;
		}
	}

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes)
	{
		if ($this->imagefile!== null && $this->image!=null) {
			if($this->oldimage!=null && $this->image!=$this->oldimage && file_exists(Yii::$app->params['avatar_abs_path'].$this->oldimage)){
				unlink(Yii::$app->params['avatar_abs_path'].$this->oldimage);
			}
			$this->imagefile->saveAs(Yii::$app->params['avatar_abs_path'].$this->image);
		}

		if (!Yii::$app->user->isGuest) {
			if($insert){
				Yii::$app->user->identity->LogActivity('user',$this->id,'create');
			}else{
				Yii::$app->user->identity->LogActivity('user',$this->id,'update');
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}
	
    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand("update ".self::tableName()." set trashed='1',trashed_at='".date("Y-m-d H:i:s")."',trashed_by='".Yii::$app->user->identity->id."' where id='".$this->id."'")->execute();
		Yii::$app->user->identity->LogActivity('user',$this->id,'delete');
		return true;
	}
}
