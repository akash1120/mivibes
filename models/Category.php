<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 * @property int $status
 */
class Category extends \yii\db\ActiveRecord
{
    public $oldimage,$imagefile;
    public $allowedImageSize = 512000;
    public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
    public $additionalFields = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes)],
            [['parent_id'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
        ];
    }

    /**
     * @return image path
     */
    public function getImageSrc()
    {

        $image=Yii::$app->params['default_avatar'];
        if($this->image!=null && file_exists(Yii::$app->params['avatar_abs_path'].$this->image)){
            $image=Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $this->image, 128, 128);
        }
        return $image;
    }

    public function beforeValidate()
    {
        //Uploading image
        if (UploadedFile::getInstance($this, 'image')) {
            $this->imagefile = UploadedFile::getInstance($this, 'image');
            // if no file was uploaded abort the upload
            if (!empty($this->imagefile) && file_exists($this->imagefile->tempName)) {
                $pInfo = pathinfo($this->imagefile->name);
                $ext = $pInfo['extension'];
                if (in_array($ext, $this->allowedImageTypes)) {
                    // Check to see if any PHP files are trying to be uploaded
                    $content = file_get_contents($this->imagefile->tempName);
                    if (preg_match('/\<\?php/i', $content)) {
                        $this->addError('image', Yii::t('app', 'Invalid file provided!'));
                        return false;
                    } else {
                        if (filesize($this->imagefile->tempName) <= $this->allowedImageSize) {
                            // generate a unique file name
                            $this->image = Yii::$app->controller->generateName() . ".{$ext}";
                        } else {
                            $this->addError('image', Yii::t('app', 'File size is too big!'));
                            return false;
                        }
                    }
                } else {
                    $this->addError('image', Yii::t('app', 'Invalid file provided!'));
                    return false;
                }
            }
        }
        if ($this->image == null && $this->oldimage != null) {
            $this->image = $this->oldimage;
        }

        if($this->parent_id <> null){

        }else{
            $this->parent_id=0;
        }
        return parent::beforeValidate();
    }
    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->imagefile!== null && $this->image!=null) {
            if($this->oldimage!=null && $this->image!=$this->oldimage && file_exists(Yii::$app->params['avatar_abs_path'].$this->oldimage)){
                unlink(Yii::$app->params['avatar_abs_path'].$this->oldimage);
            }
            $this->imagefile->saveAs(Yii::$app->params['avatar_abs_path'].$this->image);
        }
    }
    /**
     * return html status
     */
    public function getTxtStatus()
    {
        return $this->status==1 ? '<span class="label label-success">Published</span>' : '<span class="label label-warning">Pending</span>';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFields()
    {
        return $this->hasMany(CategoryFields::className(), ['category_id' => 'id']);
    }
}
