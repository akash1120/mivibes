<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%announcement}}".
 *
 * @property int $id
 * @property string $heading
 * @property string $date
 * @property string $descp
 * @property string $start_date
 * @property string $end_date
 * @property string $color_class
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 * @property int $trashed
 * @property string $trashed_at
 * @property int $trashed_by
 */
class Announcement extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%announcement}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['heading', 'date', 'descp', 'start_date', 'end_date'], 'required'],
            [['date', 'start_date', 'end_date', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['descp', 'color_class'], 'string'],
            [['created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['heading'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
			'heading' => Yii::t('app', 'Heading'),
			'date' => Yii::t('app', 'Date'),
			'descp' => Yii::t('app', 'Detail'),
			'start_date' => Yii::t('app', 'Start Date'),
			'end_date' => Yii::t('app', 'End Date'),
			'color_class' => Yii::t('app', 'Color'),
			'colorClass' => Yii::t('app', 'Color'),
			'created_by' => Yii::t('app', 'Created By'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
    }
	
    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
                    return date("Y-m-d H:i:s"); 
                },
			],
			'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
		];
     }
	 
    /**
     * return string, color class
     */
	public function getColorClass()
	{
		return Yii::$app->params['noticeClass'][$this->color_class];
	}

    /**
     * @inheritdoc
     */
	public function afterSave($insert, $changedAttributes)
	{
		if($insert){
			Yii::$app->user->identity->LogActivity('announcement',$this->id,'create');
		}else{
			Yii::$app->user->identity->LogActivity('announcement',$this->id,'update');
		}
		parent::afterSave($insert, $changedAttributes);
	}
	
    /**
     * Mark record as deleted and hides fron list.
     * @return boolean
     */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand("update ".self::tableName()." set trashed='1',trashed_at='".date("Y-m-d H:i:s")."',trashed_by='".Yii::$app->user->identity->id."' where id='".$this->id."'")->execute();
		Yii::$app->user->identity->LogActivity('announcement',$this->id,'delete');
		return true;
	}
}
