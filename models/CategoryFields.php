<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_fields".
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property string $type
 * @property string $values
 * @property string $html_class
 * @property string $html_id
 * @property int $status
 */
class CategoryFields extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_fields';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['category_id', 'status'], 'integer'],
            [['name', 'type', 'html_class', 'html_id'], 'string', 'max' => 255],
            [['values'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category ID',
            'type' => 'Type',
            'values' => 'Values',
            'html_class' => 'Html Class',
            'html_id' => 'Html ID',
            'status' => 'Status',
        ];
    }
}
