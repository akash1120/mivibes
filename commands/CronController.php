<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Deal;
use app\models\Category;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CronController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {

        $deals = Deal::find()->where(['and',['=','DATE(start_date)',date("Y-m-d")]])->andWhere(['and',['=','status',0]])->asArray()->all();
        if(isset($deals) && !empty($deals)){
            foreach ($deals as $key => $deal){
                $deal_detail = Deal::findOne($deal['id']);

                $deal_detail->status = 1;
                $deal_detail->save();
            }
        }
        $deals_exp = Deal::find()->where(['and',['=','DATE(end_date)',date("Y-m-d")]])->andWhere(['and',['=','status',1]])->asArray()->all();
        if(isset($deals_exp) && !empty($deals_exp)){
            foreach ($deals_exp as $key => $deal){
                $deal_detail = Deal::findOne($deal['id']);

                $deal_detail->status = 0;
                $deal_detail->save();
            }
        }
        echo "hello";

        /*Closed contracts end*/

    }

    public function actionContractscron()
    {
        $msg = "First line of text\nSecond line of text";

// use wordwrap() if lines are longer than 70 characters
        $msg = wordwrap($msg,70);

// send email
        mail("akash@wistech.biz","My subject",$msg);
    }
}
