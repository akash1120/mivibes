<?php

namespace app\helpers;

use Yii;
use yii\base\Component;

class HelperFunction extends Component
{
    public function convertTime($time_ago2)
    {
        $time_ago = strtotime($time_ago2);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);

        if ($seconds <= 60) {// Seconds
            return Yii::t('app', 'just now');
        } else if ($minutes <= 60) {//Minutes
            if ($minutes == 1) {
                return Yii::t('app', 'one minute ago');
            } else {
                return Yii::t('app', '{mn} minutes', ['mn' => $minutes]) . ' ' . Yii::t('app', 'ago');
            }
        } else if ($hours <= 24) {//Hours
            if ($hours == 1) {
                return Yii::t('app', 'an hour ago');
            } else {
                return Yii::t('app', '{hr} hrs ago', ['hr' => $hours]);
            }
        } else {
            return Yii::$app->formatter->asdatetime($time_ago2);
        }
    }

    public function resize($folder, $filename, $width, $height)
    {
        if (!is_file($folder . $filename)) {

            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = Yii::$app->params['cache_rel_path'] . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file($folder . $new_image) || (filectime($folder . $old_image) > filectime($folder . $new_image))) {
            $image = new ImageHelper($folder . $old_image);
            $image->resize($width, $height, 'w');
            $image->save($new_image);
        }
        return $new_image;
    }

    public function calculate_vat($amount)
    {
        return ((5 / 100) * $amount);
    }

    /**
     * @param        $date_1
     * @param        $date_2
     * @param string $differenceFormat
     * @return string
     */
    function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);

    }

    /**
     * @return mixed
     */
    public function checkAdmin()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if(Yii::$app->user->identity->user_type==0){
            return $this->redirect(['site/index']);
        }
    }
}

?>