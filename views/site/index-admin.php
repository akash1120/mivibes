<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

if (Yii::$app->user->identity->user_type != 0) {
    $this->title = 'Dashboard';
} else {
    $this->title = 'Dashboard';
}
?>
<?php
if (Yii::$app->user->identity->user_type != 0) {
    $stats = Yii::$app->controller->adminStats;
}
?>
<div class="row">
    <a href="<?= Url::to(['hotel/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-hotel"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Hotels</span>
                    <span class="info-box-number"><?= $stats['allHotels'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['deal/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-star-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Deals</span>
                    <span class="info-box-number"><?= $stats['allDeals'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['category/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-list"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Categories</span>
                    <span class="info-box-number"><?= $stats['categories'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['city/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-navy"><i class="fa fa-home"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Cities</span>
                    <span class="info-box-number"><?= $stats['cities'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <a href="<?= Url::to(['area/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-maroon"><i class="fa fa-map-marker"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Areas</span>
                    <span class="info-box-number"><?= $stats['areas'] ?></span>
                </div>
            </div>
        </div>
    </a>


</div>
