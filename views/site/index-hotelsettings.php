<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

if (Yii::$app->user->identity->user_type != 0) {
    $this->title = 'Dashboard';
} else {
    $this->title = 'Dashboard';
}
?>
<?php
    $stats = Yii::$app->controller->hotelStats;
?>
<div class="row">

    <a href="<?= Url::to(['deal/index']) ?>">
        <div class="col-xs-12 col-sm-3">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-star-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Deals</span>
                    <span class="info-box-number"><?= $stats['allDeals'] ?></span>
                </div>
            </div>
        </div>
    </a>

</div>
