<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$hotels = \app\models\User::find()->where(['user_type' => 0])->asArray()->all();
?>

<style>
    .filter select {
        height: auto !important;
    }
</style>
<div class="container">
    <div class="col-sm-4 col-sm-offset-4">
    <div class="row">
        <div class="col-lg-12 margin-top"><a href="#"><img src="<?= Yii::$app->params['siteUrl'].'/images/black-logo.jpg';?>" class="img-responsive center-block" /></a></div>
        <div class="col-lg-12 margin-top">
            <?php $form = ActiveForm::begin(['id' => 'login-form',]); ?>
            <input type="hidden" placeholder="&nbsp;" name="LoginForm[type]" value="admin">
                <label for="inp" class="inp">
                  <!--  <input type="text" id="inp" placeholder="&nbsp;">-->
                    <input type="text" id="inp" placeholder="&nbsp;" name="LoginForm[username]" required>
                    <span class="label">User</span>
                    <span class="border"></span>

                </label>
                <label for="pass" class="inp">
                    <input type="password" id="inp" placeholder="&nbsp;" name="LoginForm[password]" required>
                    <span class="label">Password</span>
                    <span class="border"></span>
                </label>
            <?php
            if(!empty($model->errors)){
                if(isset($model->errors['password'])) {
                    $message = $model->errors['password'][0];
                }else{
                    $message = $model->errors['username'][0];
                }
                ?>
                <div class="alert alert-danger" style="background:#a94442" role="alert"><?= $message; ?></div>
            <?php }
            ?>
                <div class="col-lg-12 col-md-12 margin">
                    <input class="magic-checkbox" type="checkbox" name="rememberMe" id="1" value="option">
                    <label for="1"></label>
                    <label class="pull-left text" for="1">
                        Remember Password.
                    </label>

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <input type="submit" name="" value="login">
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    </div>
</div>
