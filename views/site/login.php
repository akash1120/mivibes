<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2; // or kartik\select2\Select2
use yii\web\JsExpression;
use app\models\User;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$hotels = \app\models\User::find()->where(['user_type' => 0])->asArray()->all();
?>

<style>
    .filter select {
        height: auto !important;
    }
    .select2-selection__arrow{
        display: none !important;
    }
    .select2-selection__placeholder{
        font-weight: 800;
    }
    .select2-container--krajee .select2-selection--single .select2-selection__clear {
             right: 1rem !important;
         }
    ::-webkit-input-placeholder { /* Edge */
        font-weight: 800;
        color: #999;
        padding-left: 5px;
        font-size: 14px;
    }

    :-ms-input-placeholder { /* Internet Explorer */
        font-weight: 800;
        color: #999;
        padding-left: 5px;
        font-size: 14px;
    }

    ::placeholder {
        font-weight: 800;
        color: #999;
        padding-left: 5px;
        font-size: 14px;
    }
    .select2-container--krajee .select2-selection--single {
        height: 35px;
        padding: 10px 24px 6px 5px;
        border: none!important;
        border-bottom: 2px solid #c8ccd4 !important;
        box-shadow: none !important;
        border-radius: 0px !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-top"><a href="#"><img src="<?= Yii::$app->params['siteUrl'].'/images/black-logo.jpg';?>" class="img-responsive center-block" /></a></div>
        <div class="col-lg-12 margin-top">
            <?php $form = ActiveForm::begin(['id' => 'login-form',]); ?>
            <?php

            $url = \yii\helpers\Url::to(['user-list']);
            echo $form->field($model, 'username')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Type Hotel Name ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 0,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ])->label(false); ?>

            <input type="hidden" placeholder="&nbsp;" name="LoginForm[type]" value="login">
                <label for="pass" class="inp">
                    <input type="password" id="inp" placeholder="Password" name="LoginForm[password]" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'"  required>
                    <span class="label"></span>
                    <span class="border"></span>
                </label>
            <?php if(!empty($model->errors)){
                if(isset($model->errors['password'])) {
                    $message = $model->errors['password'][0];
                }else{
                    $message = $model->errors['username'][0];
                }
                ?>

                <div class="alert alert-danger text-left" style="background:#a94442" role="alert"><?= $message ?></div>
                <div class="clearfix"></div>
            <?php } ?>
                <div class="col-lg-12 col-md-12 margin">
                    <input class="magic-checkbox" type="checkbox" name="rememberMe" id="1" value="option">
                    <label for="1"></label>
                    <label class="pull-left text" for="1">
                        Remember Password.
                    </label>

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <input type="submit" name="" value="login">
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
