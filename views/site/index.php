<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\widgets\CustomGridView;
use yii\widgets\ListView;


/* @var $this yii\web\View */

$this->title = 'Categories';
?>

<!--<div class="row">
    <div class="col-xs-12 col-sm-4">

<ul class="pro">
    <li style="margin-bottom: 20px">

        <div class="row">
            <a href="<?/*= Yii::$app->params['siteUrl'].'deal/filter-results?type=hot'*/?>">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7"><h1>Hot Deals</h1></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5"><img src="images/cache/0433974001569237984-150x150.png" class="img-responsive center-block"></div>
            </a>
        </div>
    </li>
</ul>
    </div>

</div>-->

    <?php
    if($searchModel!=null){
        echo ListView::widget( [
            'dataProvider' => $dataProvider,
            'viewParams' => ['id'=>$searchModel->id,'title'=>$searchModel->title,'image'=>$searchModel->image],
            'itemOptions' => ['class' => 'col-xs-12 col-sm-4'],
            'itemView' => '/site/_item',
            'layout'=>"<div class=\"row\">{items}</div>",
            'emptyText' => '<div class="alert alert-danger">No results found!</div>'
        ] );
    }
    ?>


