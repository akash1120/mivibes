<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\widgets\CustomGridView;
use yii\widgets\ListView;


/* @var $this yii\web\View */
$image = Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $model->image, 150, 150);
if ($model->image <> null) {
    $image = Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $model->image, 150, 150);
} else {
    $image = Yii::$app->params['default_deal'];
    $image = Yii::$app->helperFunction->resize('images/', 'food.png', 150, 150);
}
if ($model->id == 200) {
    $url = Yii::$app->params['siteUrl'] . 'deal/filter-results?type=2';
} else {
    $url = Yii::$app->params['siteUrl'] . 'deal/filter?id=' . $model->id;
}

$cats = \app\models\Category::find()->where(['parent_id' => 0])->andWhere(['status' => 1])->asArray()->all();
$cats_array = array();
foreach ($cats as $cat) {
    $cats_array[] = $cat['id'];
}
$key = array_search($model->id, $cats_array);

?>
<div class="container-fluid">
    <div class="row">
        <ul class="pro">
            <li style="margin-bottom: 20px">

                <div class="row">
                    <a href="<?= $url ?>">

                        <?php
                        if ($key % 2 == 0) {

                            ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7"><h1><?= $model->title; ?></h1></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5"><img src="<?= $image; ?>"
                                                                                  class="img-responsive center-block"/>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-5"><img src="<?= $image; ?>"
                                                                                  class="img-responsive center-block"/>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7" style="text-align: right;">
                                <h1><?= $model->title; ?></h1></div>

                            <?php
                        }
                        ?>


                    </a>
                </div>
            </li>
        </ul>
    </div>

</div>
