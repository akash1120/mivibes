<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */

$this->title = Yii::t('app', 'Update Announcement: {nameAttribute}', [
    'nameAttribute' => $model->heading,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Announcements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->heading, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="addon-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
