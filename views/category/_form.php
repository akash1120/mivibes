<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form box">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(\app\models\Category::find()->where(['status'=>1])->orderBy('title')->asArray()->all(),'id','title'),array('prompt'=>'Select Parent Category')) ?>
        </div>
        <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?>
        </div>
        <div class="col-xs-6 col-sm-6">
            <div class="col-sm-6">
                <?= $form->field($model, 'image')->fileInput()->label('Image') ?>
            </div>
            <?php if ($model->imageSrc != null && $model->imageSrc != Yii::$app->params['default_avatar']) { ?>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label"><?= Yii::t('app', 'Image') ?></label>
                        <img src="<?= $model->imageSrc ?>" width="128"/>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="form-group col-xs-12 col-sm-12">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
