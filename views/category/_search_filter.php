<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .filter select {
        height: auto !important;
    }
</style>
<!--<script>
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 0, 500 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "AED " + ui.values[ 0 ] + " - AED " + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "AED " + $( "#slider-range" ).slider( "values", 0 ) +
            " - AED " + $( "#slider-range" ).slider( "values", 1 ) );
    } );
</script>-->

<div class="category-search">




    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin"><img src="images/food.png" class="img-responsive center-block" /></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin">
                <?php $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'class' => 'filter'
                    ]
                ]); ?>
                <?= $form->field($model, 'hotel_id')->dropDownList(ArrayHelper::map(user::find()->where(['status' => 1, 'user_type' => 0])->orderBy('name')->asArray()->all(), 'id', 'name'), array('prompt'=>'By Hotel'))->label(false); ?>
                <?= $form->field($model, 'cuisine')->textInput(['maxlength' => true,'placeholder'=>'By Cuisine'])->label(false); ?>
                <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(\app\models\City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title'))->label(false); ?>
                <?= $form->field($model, 'area_id')->dropDownList(ArrayHelper::map(\app\models\Area::find()->where(['status'=>1])->orderBy('name')->asArray()->all(),'id','name'))->label(false); ?>
                <?= $form->field($model, 'category_id')->hiddenInput(['value'=> $category_id])->label(false); ?>
                  <!--  <p class="price">
                        <label for="amount">Price range:</label>
                    </p>
                    <div id="slider-range"></div>
                    <input type="text" id="amount" readonly style="border:0; color:#333333; font-weight:noraml;">-->
                    <input type="submit" name="" value="Filter">

                        <span class="showall">
                            <a href="<?= Yii::$app->params['siteUrl'].'/deals';?>">
                            Show All
                                 </a>
                        </span>


                <?php ActiveForm::end(); ?>
            </div>
        </div>






</div>
