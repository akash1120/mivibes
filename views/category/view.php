<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use app\widgets\CustomGridView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$result = \app\models\Category::find()->where(['parent_id' => $model->id])->asArray()->all();

?>
<div class="category-view box">

    <div class="box-header with-border">
        <h3></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Update']) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Back']) ?>
        </div>
    </div>
    <div class="box-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
            ],
        ]) ?>


    </div>
</div>
<div class="category-view box">
    <?php if(count($result) > 0){ ?>
    <div class="box-body">
        <h3>Sub Categories</h3>

        <?= CustomGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
                /*['class' => 'yii\grid\ActionColumn'],*/
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>Yii::t('app','Actions'),
                    'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                    'contentOptions'=>['class'=>'noprint text-right'],
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<i class="fa fa-folder"></i>', ['view','id'=>$model->id], [
                                'title' => Yii::t('app', 'View'),
                                'data-toggle'=>'tooltip',
                                'class'=>'btn btn-info btn-xs',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-edit"></i>', ['update','id'=>$model->id], [
                                'title' => Yii::t('app', 'Update'),
                                'data-toggle'=>'tooltip',
                                'class'=>'btn btn-success btn-xs',
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            /*return Html::a('<i class="fa fa-remove"></i>', $url, [
                                'title' => Yii::t('app', 'Delete'),
                                'data-toggle'=>'tooltip',
                                'data-method'=>'post',
                                'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                                'class'=>'btn btn-danger btn-xs',
                            ]);*/
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
    <?php } ?>


</div>

