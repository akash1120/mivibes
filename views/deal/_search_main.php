<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .filter select {
        height: auto !important;
    }
    .select2-container{
        margin-bottom: 20px !important;
    }
    #dealsearch-title{
        border-radius: 0px!important;
        height: 38px !important;
    }
    .select2-selection--single{
        border-radius: 5px !important;
        height: 38px !important;
    }
    .select2-container--krajee .select2-selection--single {
        height: 40px;
        padding: 12px 24px 6px 5px;
        /* border: none!important;
         border-bottom: 2px solid #c8ccd4 !important;
         box-shadow: none !important;
         border-radius: 0px !important;*/
    }
</style>

<div class="category-search">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search-page">
                <a href="<?= Yii::$app->params['siteUrl'];?>"><i class="fa fa-times"></i></a>
                <?php $form = ActiveForm::begin([
                    'action' => ['search-results'],
                    'method' => 'get',
                    'options' => [
                        'class' => 'sreach-from'
                    ]
                ]); ?>


                <?= \kartik\select2\Select2::widget([
                    'model' => $model,
                    'attribute' => 'hotel_id',
                    'data' => \yii\helpers\ArrayHelper::map(User::find()->where(['status' => 1, 'user_type' => 0])->orderBy('name')->all(), 'id', function ($model) {
                        //return $model->first_name . " " . $model->last_name;
                        return $model->name;
                    }),
                    'options' => ['placeholder' => 'By Hotel ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>


                <?= \kartik\select2\Select2::widget([
                    'model' => $model,
                    'attribute' => 'category_id',
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Category::find()->where(['status' => 1, 'parent_id' => 3])->orderBy('title')->all(), 'id', function ($model) {
                        //return $model->first_name . " " . $model->last_name;
                        return $model->title;
                    }),
                    'options' => ['placeholder' => 'By Category ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true,'placeholder'=>'Enter Your Keyword Here...'])->label(false); ?>
                    <input type="submit" name="" value="Search">
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</div>
