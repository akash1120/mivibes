<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\CustomGridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin">
            <div class="tags">

                <?php
                $filter_check = 0;


                if (isset($datafilters['subcategory']) && $datafilters['subcategory'] <> null) {
                    $filter_check = 1;
                    $cat_data = \app\models\Category::findOne($datafilters['subcategory']);
                    echo '<span class="label label-info">' . substr($cat_data->title, 0, 10) . '.. <a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=subcategory&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';
                }
                if (isset($datafilters['sub_category']) && $datafilters['sub_category'] <> null) {
                    $filter_check = 1;
                    $cat_data = \app\models\Category::findOne($datafilters['sub_category']);
                    echo '<span class="label label-info">' . substr($cat_data->title, 0, 10) . '.. <a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=sub_category&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';
                }

                if (isset($datafilters['cities']) && $datafilters['cities'] <> null) {
                    $filter_check = 1;
                    echo '<span class="label label-info">' . substr($datafilters['cities'], 0, 10) . '.. <a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=cities&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';
                }
                if (isset($datafilters['title']) && $datafilters['title'] <> null) {
                    $filter_check = 1;
                    echo '<span class="label label-info">' . substr($datafilters['title'], 0, 10) . '.. <a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=title&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';
                }

                if (isset($datafilters['hotel_id']) && $datafilters['hotel_id'] <> null) {
                    $filter_check = 1;
                    $hotel_data = \app\models\User::findOne($datafilters['hotel_id']);
                    echo '<span class="label label-info">' . substr($hotel_data->name, 0, 10) . '.. <a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=hotel_id&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';
                }
                if (isset($datafilters['cuisine']) && $datafilters['cuisine'] <> null) {
                    $filter_check = 1;
                    echo '<span class="label label-info">' . $datafilters['cuisine'] . '<a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=cuisine&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';;
                }
                if (isset($datafilters['city_id']) && $datafilters['city_id'] <> null) {
                    $filter_check = 1;
                    $city_data = \app\models\City::findOne($datafilters['city_id']);
                    echo '<span class="label label-info">' . $city_data->title . '<a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=city_id&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';
                }
                if (isset($datafilters['area_id']) && $datafilters['area_id'] <> null) {
                    $filter_check = 1;
                    $area_data = \app\models\Area::findOne($datafilters['area_id']);
                    echo '<span class="label label-info">' . $area_data->name . '<a style="color: white" href="' . Yii::$app->params['siteUrl'] . 'deal/filter-results?id=' . $datafilters['category_id'] . '&remove_filter=area_id&' . http_build_query($datafilters) . '"><i class="fa fa-times"></i></a></span>';
                }
                if (isset($datafilters['hot_deal']) && $datafilters['hot_deal'] <> null) {
                    $filter_check = 1;
                    ?>
                    <span class="label label-info" style="background-color: red !important;">Hot deals <a style="color: white;"
                                                                href="<?= Yii::$app->params['siteUrl'] ?>"><i
                                    class="fa fa-times"></i></a></span>
                    <?php
                }
                if ($filter_check == 0) {
                    ?>
                    <span class="label label-info">All deals <a style="color: white"
                                                                href="<?= Yii::$app->params['siteUrl'] . 'deal/filter?id=' . $datafilters['category_id'] ?>"><i
                                    class="fa fa-times"></i></a></span>

                    <?php
                }
                ?>

            </div>
            <?php
            if ($searchModel != null) {
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'viewParams' => ['id' => $searchModel->id, 'title' => $searchModel->title, 'image' => $searchModel->image],
                    'itemOptions' => ['class' => 'col-xs-12 col-sm-4'],
                    'itemView' => '/deal/_item',
                    'layout' => "<div class=\"row\">{items}</div>\n{pager}",
                    'emptyText' => '<div class="alert alert-danger">No results found!</div>'
                ]);
            }
            ?>


        </div>
    </div>
</div>