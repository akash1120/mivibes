<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use app\models\DealFields;

/* @var $this yii\web\View */
/* @var $model app\models\Deal */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Deals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

if($model->image <> null) {
    $image = Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $model->image, 300,300);
}else{
    $image =  Yii::$app->params['default_deal'];
    $image =  Yii::$app->helperFunction->resize('images/', 'food.png', 300, 200);
}
$image2='';
$image3='';

if($model->image2 <> null) {
    $image2 = Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $model->image2, 300,300);
}
if($model->image3 <> null) {
    $image3 = Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $model->image3, 300,300);
}

if($model->category_id == 1) {
    $rest_name = DealFields::find()->where([
        'deal_id' => $model->id,
        'category_filed_id' => 1
    ])->one();
}else if($model->category_id == 2) {
    $rest_name = DealFields::find()->where([
        'deal_id' => $model->id,
        'category_filed_id' => 3
    ])->one();
}
if(isset($rest_name->value) && $rest_name->value <> null) {
    $rest_name_value = $rest_name->value;
}else{
    $rest_name_value = 'Deal';
}
?>
<!-- mainwrapper -->
<div class="container-fluid">

    <div class="row">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <?php if( isset($image2) &&  $image2 <> null){ ?>
                <li data-target="#myCarousel" data-slide-to="1"></li>
               <?php } ?>
                <?php if( isset($image3) &&  $image3 <> null){ ?>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <?php } ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="<?= $image; ?>" alt="Los Angeles" style="width:100%;">
                </div>
                <?php if( isset($image2) &&  $image2 <> null){ ?>
                <div class="item">
                    <img src="<?= $image2; ?>" alt="Los Angeles" style="width:100%;">
                </div>
                <?php } ?>
                <?php if( isset($image3) &&  $image3 <> null){ ?>
                <div class="item">
                    <img src="<?= $image3; ?>" alt="Los Angeles" style="width:100%;">
                </div>
                <?php } ?>

                <!--<div class="item">
                    <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago" style="width:100%;">
                </div>

                <div class="item">
                    <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="New york" style="width:100%;">
                </div>-->
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin">

            <!-- search-ruslt -->
            <div class="padding">
                <h1><?= $rest_name_value; ?></h1>
               <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, enim tempore quo aspernatu rem ipsum dolor...</p>-->
              <?php if($model->discount <> null){?>
                <span class="tag-detail"><?= $model->discount; ?>% Discount</span>
                <?php } ?>
                <?php if($model->category_id == 2){?>
                <span class="tag-detail" style="background:red">Hot Deal</span>
                <?php } ?>
          <!--      <span class="tag-fav">Add To Favorites</span>-->

                <?php if($model->discount_description <> null){?>
                <div class="valid" style="overflow-wrap: break-word;">
                    <h1>Discount Detail</h1>
                    <p> <?= trim($model->discount_description) ?></p>
                </div>
                    <hr>
                <?php } ?>
                <?php
                if ($model->end_date <> null && $model->category->parent_id == 3) {
                    $originalDate = $model->end_date;
                    $newDate = date("d-m-Y", strtotime($originalDate));
                    ?>
                    <p>Expiry Date : <?= $newDate; ?></p>
                    <hr>
                    <?php
                }
                ?>
                <?php
                if ($model->end_date <> null && $model->category_id == 2) {
                    $originalDate = $model->end_date;
                    $newDate = date("d-m-Y", strtotime($originalDate));
                    ?>
                    <p>Expiry Date : <?= $newDate; ?></p>
                    <hr>
                    <?php
                }
                ?>

                <?php if($model->email <> null){?>
                    <p style="padding-top: 10px"><strong>Email :</strong>  <?= $model->email; ?></p>
                <?php } ?>

                <?php if($model->phone <> null){?>
                    <p><strong>Phone :</strong>  <?= $model->phone; ?></p>
                <?php } ?>

                <div class="valid" style="overflow-wrap: break-word;">
                    <strong>Terms & Conditions</strong>
                    <?= $model->terms; ?>
                </div>
                <hr>

                <?php
                if ($model->cities <> null && $model->category->parent_id == 3) {
                    ?>
                    <div class="location"><i class="fa fa-map-marker"></i>

                        <?php if($model->cities == 'uae'){
                            echo "All Over UAE";
                        }else{
                            echo ucfirst($model->cities);
                        }
                        ?>

                    </div>
                <?php }else { ?>
                    <div class="location"><i class="fa fa-building-o"></i> <?= $model->hotel->name ?></div>
                    <br>
                    <?php
                    if (isset($model->hotel->city->title) && $model->hotel->city->title <> null) {
                        ?>
                        <div class="location"><i class="fa fa-map-marker"></i> <?= $model->hotel->city->title ?></div>
                        <?php
                    }
                }
                ?>

                <h1>Description</h1>
                <div class="discount">

                    <?= $model->description ?>
                   <!-- <input type="submit" name="" class="grab" value="Grab This Offer">-->
                </div>

            </div>

            <!-- search-ruslt -->


        </div>
    </div>

</div>
<!-- mainwrapper -->
