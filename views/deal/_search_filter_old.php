<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\models\City;
use yii\helpers\Url;
use app\assetfiles\DatePickerAsset;
DatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .filter select {
        height: auto !important;
    }
</style>
<!--<script>
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 0, 500 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "AED " + ui.values[ 0 ] + " - AED " + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "AED " + $( "#slider-range" ).slider( "values", 0 ) +
            " - AED " + $( "#slider-range" ).slider( "values", 1 ) );
    } );
</script>-->

<div class="category-search">




    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin"><img src="images/food.png" class="img-responsive center-block" /></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin">
                <?php $form = ActiveForm::begin([
                    'action' => ['filter-results'],
                    'method' => 'get',
                    'options' => [
                        'class' => 'filter'
                    ]
                ]); ?>
                <?php if($category_id != 3){ ?>


                        <?= \kartik\select2\Select2::widget([
                            'model' => $model,
                            'attribute' => 'hotel_id',
                            'data' => \yii\helpers\ArrayHelper::map(User::find()->where(['status' => 1, 'user_type' => 0])->orderBy('name')->all(), 'id', function ($model) {
                                //return $model->first_name . " " . $model->last_name;
                                return $model->name;
                            }),
                            'options' => ['placeholder' => 'By Hotel ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>





                   <!-- --><?/*= $form->field($model, 'hotel_id')->dropDownList(ArrayHelper::map(user::find()->where(['status' => 1, 'user_type' => 0])->orderBy('name')->asArray()->all(), 'id', 'name'), array('prompt'=>'By Hotel'))->label(false); */?>
               <?php }else{ ?>
                    <?= $form->field($model, 'subcategory')->dropDownList(ArrayHelper::map(\app\models\Category::find()->where(['status' => 1, 'parent_id' => 3])->orderBy('title')->asArray()->all(), 'id', 'title'), array('prompt'=>'By Category'))->label(false); ?>
               <?php } ?>


                <?php if($category_id != 3){ ?>
                <?= $form->field($model, 'cuisine')->textInput(['maxlength' => true,'placeholder'=>'By Cuisine'])->label(false); ?>


                <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(\app\models\City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title'),array('prompt'=>'By City'))->label(false); ?>

                <select id="dealsearch-area_id" class="form-control" name="DealSearch[area_id]" aria-invalid="false">
                    <option value=''>By Area</option>
                </select>



                <?php }else{ ?>
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true,'placeholder'=>'By Title'])->label(false); ?>

                    <select id="deal-cities" class="form-control" name="DealSearch[cities]" aria-invalid="false">
                        <option value="uae">All Over UAE</option>
                        <option value="dubai" <?php if (isset($model->cities) && $model->cities == 'dubai') { echo "selected";} ?>>Dubai</option>
                        <option value="abu dabhi" <?php if (isset($model->cities) && $model->cities == 'abu dabhi') { echo "selected";} ?>>Abu Dhabi</option>
                        <option value="sharjah" <?php if (isset($model->cities) && $model->cities == 'sharjah') { echo "selected";} ?>>Sharjah</option>
                        <option value="al ain" <?php if (isset($model->cities) && $model->cities == 'al ain') { echo "selected";} ?>>Al Ain</option>
                        <option value="ajman" <?php if (isset($model->cities) && $model->cities == 'ajman') { echo "selected";} ?>>Ajman</option>
                        <option value="ras al khaimah" <?php if (isset($model->cities) && $model->cities == 'ras al khaimah') { echo "selected";} ?>>Ras Al Khaimah</option>
                        <option value="umm al quwain" <?php if (isset($model->cities) && $model->cities == 'umm al quwain') { echo "selected";} ?>>Umm Al Quwain</option>
                    </select>



                <?php } ?>
                <?= $form->field($model, 'category_id')->hiddenInput(['value'=> $category_id])->label(false); ?>
                  <!--  <p class="price">
                        <label for="amount">Price range:</label>
                    </p>
                    <div id="slider-range"></div>
                    <input type="text" id="amount" readonly style="border:0; color:#333333; font-weight:noraml;">-->
                    <input type="submit" name="" value="Filter">

                        <span class="showall">
                            <a href="<?= Yii::$app->params['siteUrl'].'deal/filter-results?type='.$category_id;?>">
                            Show All
                                 </a>
                        </span>


                <?php ActiveForm::end(); ?>
            </div>
        </div>






</div>
<?= $this->registerJs('



        $("body").on("change", "#dealsearch-city_id", function(e) {
            var city_id = $(this).val();
            $.ajax({
                url:  "' . Url::base(true) . '/hotel/getareasofcity?id="+city_id,
                type: "GET",
                success : function(res){
                     var obj = JSON.parse(res);
                      if(obj.options) {
                        $("#dealsearch-area_id").html(obj.options);
                    }
                     

                },
                error : function(data){
                    console.log(data);
                }
            });
        });
'); ?>