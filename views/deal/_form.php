<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;
use app\assetfiles\DatePickerAsset;
use app\models\User;
use dosamigos\tinymce\TinyMce;
use yii\web\JqueryAsset;

DatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Deal */
/* @var $form yii\widgets\ActiveForm */
$this->registerCssFile('@web/'.'plugins/datepicker/datepicker3.css', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/'.'plugins/datepicker/bootstrap-datepicker.js', ['depends' => [JqueryAsset::className()]]);

$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
');
?>


<style>
    input[type=checkbox]#hot_deal {
        display: inline-block!important;
    }
</style>

<div class="deal-form">
    <div class="box box-success">

        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            <div class="row">
                <?php if (isset($model->category_id) && $model->category_id <> null) { ?>

                    <?= $form->field($model, 'category_id')->hiddenInput(['value' => $model->category_id])->label(false); ?>
                <?php } else { ?>
                    <?= $form->field($model, 'category_id')->hiddenInput(['value' => 2])->label(false); ?>

                <?php } ?>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
                <?php if($model->leisure_deal == 0){ ?>
                <div class="col-xs-12 col-sm-6">

                <?php if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) { ?>
                    <?= $form->field($model, 'hotel_id')->dropDownList(ArrayHelper::map(user::find()->where(['status' => 1, 'user_type' => 0])->orderBy('name')->asArray()->all(), 'id', 'name'))->label('Hotel'); ?>
                    <?php
                }else{
                    $model->hotel_id = Yii::$app->user->identity->id;
                    ?>
                        <label class="control-label" for="deal-title">Hotel</label>
                        <input type="text"  class="form-control" value="<?= $model->hotel->name; ?>" maxlength="255" readonly>
                    <?= $form->field($model, 'hotel_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false); ?>
                <?php } ?>

                </div>
                <?php } else{ ?>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group field-deal-cities has-success">
                            <label class="control-label" for="deal-cities">City</label>
                            <select id="deal-cities" class="form-control" name="Deal[cities]" aria-invalid="false">
                                <option value="">By City</option>
                                <option value="uae" <?php if (isset($model->cities) && $model->cities == 'uae') { echo "selected";} ?>>All Over UAE</option>
                                <option value="dubai" <?php if (isset($model->cities) && $model->cities == 'dubai') { echo "selected";} ?>>Dubai</option>
                                <option value="abu dabhi" <?php if (isset($model->cities) && $model->cities == 'abu dabhi') { echo "selected";} ?>>Abu Dhabi</option>
                                <option value="sharjah" <?php if (isset($model->cities) && $model->cities == 'sharjah') { echo "selected";} ?>>Sharjah</option>
                                <option value="al ain" <?php if (isset($model->cities) && $model->cities == 'al ain') { echo "selected";} ?>>Al Ain</option>
                                <option value="ajman" <?php if (isset($model->cities) && $model->cities == 'ajman') { echo "selected";} ?>>Ajman</option>
                                <option value="ras al khaimah" <?php if (isset($model->cities) && $model->cities == 'ras al khaimah') { echo "selected";} ?>>Ras Al Khaimah</option>
                                <option value="umm al quwain" <?php if (isset($model->cities) && $model->cities == 'umm al quwain') { echo "selected";} ?>>Umm Al Quwain</option>
                            </select>

                            <div class="help-block"></div>
                        </div>
                    </div>




                        <div class="col-sm-6">
                            <?= $form->field($model, 'start_date')->textInput(['class'=>'form-control dtpicker']) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'end_date')->textInput(['class'=>'form-control dtpicker']) ?>
                        </div>


                <?php } ?>
                <?php if($model->leisure_deal == 0 && $model->category_id == 2){ ?>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'start_date')->textInput(['class'=>'form-control dtpicker']) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'end_date')->textInput(['class'=>'form-control dtpicker']) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'sub_category')->dropDownList(ArrayHelper::map(\app\models\Category::find()->where(['status' => 1, 'parent_id' => 2])->orderBy('title')->asArray()->all(), 'id', 'title'), ['prompt' => 'Select Category'])->label('Category'); ?>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
                <?php if($model->leisure_deal == 0){ ?>
                <?php if (isset($model->category->categoryFields) && $model->category->categoryFields <> null) { ?>
                    <!-- <h3>Category Fields</h3>-->

                    <!--  <div class="row">-->
                    <?php

                    foreach ($model->category->categoryFields as $key => $field) { ?>

                        <input type="hidden" name="Deal[additionalFields][<?= $key ?>][category_filed_id]"
                               class="form-control"
                               placeholder="<?= $field->id ?>"
                               value="<?= $field->id ?>"
                        >

                        <?php
                        $selectValue = $model->getFieldByTypeFiled($field->id);

                        ?>
                        <div class="<?= $field->html_class ?>">

                            <div class="form-group">
                                <label><?= $field->name ?></label>

                                <?php if ($field->type == 'select') { ?>
                                    <select name="Deal[additionalFields][<?= $key ?>][value]" class="form-control">
                                        <?php foreach (explode(',', $field->values) as $key => $value) { ?>
                                            <?php if ($selectValue !== null) { ?>
                                                <option value="<?= $value ?>" selected><?= $value ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $value ?>"><?= $value ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>

                                <?php } elseif ($field->type == 'input') { ?>
                                    <input name="Deal[additionalFields][<?= $key ?>][value]" class="form-control"
                                           type="text"
                                           placeholder="<?= $field->name ?>"
                                           value="<?= $selectValue !== null ? $selectValue->value : '' ?>">
                                <?php } elseif ($field->type == 'textarea') { ?>
                                    <textarea name="Deal[additionalFields][<?= $key ?>][value]" class="form-control"
                                              placeholder="<?= $field->name ?>"> <?= $selectValue !== null ? $selectValue->value : '' ?> </textarea>
                                <?php } ?>


                            </div>

                        </div>
                    <?php } ?>

                    <!-- </div>-->
                <?php } ?>
                    <?php } ?>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6" style="padding-bottom: 20px">
                    <label class="control-label" for="deal-discount">Discount Detail</label>
                <textarea name="Deal[discount_description]" class="form-control"
                          placeholder="<?= $field->name ?>"> <?= $model->discount_description !== null ? trim($model->discount_description) : '' ?> </textarea>
                </div>
                <br>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'discount')->textInput()->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'map_link')->textInput(['class'=>'form-control']) ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-12">


                    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6],
                        'language' => 'en',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
                        ]
                    ]); ?>


                </div>
                <div class="col-xs-12 col-sm-12">

                    <?= $form->field($model, 'terms')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6],
                        'language' => 'en',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent"
                        ]
                    ]); ?>

                </div>

               <!-- <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6" style="padding-bottom: 50px">
                    <input type="hidden"  class="form-control" name="Deal[hot_deal]" value="<?/*= $model->hot_deal; */?>">
                    <label for="hot-deal">
                        <input type="checkbox" id="hot_deal" name="Deal[hot_check]"
                               <?php /*if($model->hot_deal == 1){echo "checked";} */?>
                       />
                        Make Hot Deal
                    </label>
                </div>-->

                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true])->textInput(['type' => 'email']) ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
                </div>


            </div>
            <!-- <div class="row">
                <div class="col-sm-12">
                    <h3>Category fields</h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <? /*= $form->field($model, 'rest_name')->textInput(['maxlength' => true]) */ ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <? /*= $form->field($model, 'cuisine')->textInput(['maxlength' => true]) */ ?>
                </div>
            </div>-->


            <div class="col-xs-12 col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'image')->fileInput()->label('Update Image 1') ?>
                </div>
                <?php if ($model->imageSrc != null && $model->imageSrc != Yii::$app->params['default_avatar']) { ?>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <img src="<?= $model->imageSrc ?>" width="128"/>
                        </div>
                    </div>
                <?php } ?>
                <hr>
            </div>


            <div class="col-xs-12 col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'image2')->fileInput()->label('Update Image 2') ?>
                </div>
                <?php if ($model->imageSrc2 != null && $model->imageSrc2 != Yii::$app->params['default_avatar']) { ?>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <img src="<?= $model->imageSrc2 ?>" width="128"/>
                        </div>
                    </div>
                <?php } ?>
                <hr>
            </div>


            <div class="col-xs-12 col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'image3')->fileInput()->label('Update Image 3') ?>
                </div>
                <?php if ($model->imageSrc3 != null && $model->imageSrc3 != Yii::$app->params['default_avatar']) { ?>
                    <div class="col-sm-6">
                        <div class="form-group">

                            <img src="<?= $model->imageSrc3 ?>" width="128"/>
                        </div>
                    </div>
                <?php } ?>
            </div>


        </div>
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
