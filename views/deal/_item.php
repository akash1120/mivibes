<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\widgets\CustomGridView;
use yii\widgets\ListView;
use app\models\Deal;
use app\models\DealFields;


/* @var $this yii\web\View */
$model_data = Deal::findOne($model->id);
if ($model->image <> null) {
    $image = Yii::$app->helperFunction->resize(Yii::$app->params['avatar_abs_path'], $model->image, 300, 200);
} else {
    $image = Yii::$app->params['default_deal'];
    if ($model->category_id < 3) {
        $image = Yii::$app->helperFunction->resize('images/', 'food.png', 300, 200);
    }else{
        $image = '';
    }
}

if ($model->category_id == 1) {
    $rest_name = DealFields::find()->where([
        'deal_id' => $model->id,
        'category_filed_id' => 1
    ])->one();
} else if ($model->category_id == 2) {
    $rest_name = DealFields::find()->where([
        'deal_id' => $model->id,
        'category_filed_id' => 3
    ])->one();
}
if (isset($rest_name->value) && $rest_name->value <> null) {
    $rest_name_value = $rest_name->value;
} else {
   // $rest_name_value = 'Deal';
    $rest_name_value = $model->title;;
}


?>
<style>
    .item-minheight {
        min-height: 540px;
    }
</style>
<!-- search-ruslt -->
<div class="search-ruslt item-minheight">
    <a href="<?= Yii::$app->params['siteUrl'] . 'deal/viewdeal?id=' . $model_data->id; ?>">
        <?php if ($model_data->discount <> null) { ?>
            <span class="label-flag"><?= $model_data->discount ?>% OFF</span>
        <?php } ?>
        <img src="<?= $image; ?>" class="img-responsive center-block"/>
    </a>
    <div class="padding">
        <a href="<?= Yii::$app->params['siteUrl'] . 'deal/viewdeal?id=' . $model_data->id; ?>">
            <h1><?= $rest_name_value ?></h1></a>
        <?php
        if ($model_data->end_date <> null && $model->category->parent_id == 3) {
            $originalDate = $model_data->end_date;
            $newDate = date("d-m-Y", strtotime($originalDate));
            ?>
            <p>Expiry Date : <?= $newDate; ?></p>
            <?php
        }
        ?>
        <?php
        if ($model_data->end_date <> null && $model->category_id == 2) {
            $originalDate = $model_data->end_date;
            $newDate = date("d-m-Y", strtotime($originalDate));
            ?>
            <p>Expiry Date : <?= $newDate; ?></p>
            <?php
        }
        ?>
        <p><?= substr(strip_tags($model_data->description, '<p><a><ul></ul></a><li></li>'), 0, 60) . ' ...'; ?></p>
        <?php
        if ($model_data->cities <> null && $model->category->parent_id == 3) {
        ?>
        <div class="location">
            <?php if (isset($model_data->map_link) && $model_data->map_link <> null) {
                ?>

                <a style="text-decoration: none;" href="<?= $model_data->map_link; ?>" target="_blank">
                    <i class="fa fa-map-marker"></i>
                </a>
            <?php }else{ ?>
                <i class="fa fa-map-marker"></i>
            <?php } ?>


            </i>

            <?php if($model_data->cities == 'uae'){
                echo "All Over UAE";
            }else{
                echo ucfirst($model_data->cities);
            }
            ?>

        </div>
        <?php }else { ?>
           <!-- --><?php /*if ($model->category_id == 2) { */?>

                <div class="location"><i class="fa fa-building-o"></i> <?= $model_data->hotel->name ?></div>
                <br>
          <!--  --><?php /* } */?>
            <?php
            if (isset($model_data->hotel->city->title) && $model_data->hotel->city->title <> null) {
                ?>
                <div class="location">
               <?php if (isset($model_data->map_link) && $model_data->map_link <> null) {
                ?>

                <a style="text-decoration: none;" href="<?= $model_data->map_link; ?>" target="_blank">
                    <i class="fa fa-map-marker"></i>
                </a>
            <?php }else{ ?>
                <i class="fa fa-map-marker"></i>
            <?php } ?>

                    <?= $model_data->hotel->city->title ?></div>
                <?php
            }
        }
        ?>
        <?php if (isset($model_data->website) && $model_data->website <> null) {
            ?>
<br>
        <div class="location">

            <?php

            $website_url = $model_data->website;
            $url_web = parse_url($model_data->website);
            if(isset($url_web['scheme']) && ($url_web['scheme'] == 'http' || $url_web['scheme'] == 'https')){
            }else{

                $website_url = 'http://'.$website_url;
            }

            ?>
            <a style="text-decoration: none;" href="<?= $website_url ?>" target="_blank">
                <i class="fa fa-globe"></i>
                Visit on Website
            </a>
        </div>
        <?php } ?>
        <?php if (isset($model_data->facebook) && $model_data->facebook <> null) {
            ?>
            <br>
        <div class="location">
            <?php

            $facebook_url = $model_data->facebook;
            $url_fb = parse_url($model_data->facebook);
            if(isset($url_fb['scheme']) && ($url_fb['scheme'] == 'http' || $url_fb['scheme'] == 'https')){
            }else{

                $facebook_url = 'http://'.$facebook_url;
            }

            ?>
            <a style="text-decoration: none;" href="<?= $facebook_url; ?>" target="_blank">
                <i class="fa fa-facebook"></i>
                 &nbsp;Visit on Facebook
            </a>
        </div>
        <?php } ?>

        <!--<div class="location"><i class="fa fa-map-marker"></i> <?/*= $model_data->hotel->city->title */?></div>-->
    </div>
</div>
<!-- search-ruslt -->


<!--<div class="container-fluid">
    <div class="row">
        <ul class="pro">
            <li>

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><a href="<? /*= Yii::$app->params['siteUrl'].'deal/filter?id='.$model->id;*/ ?>"><h1><? /*= $model->title */ ?></h1></a></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><img src="<? /*= $image;*/ ?>"
                                                                          class="img-responsive center-block"/></div>
                </div>
            </li>
        </ul>
    </div>

</div>-->
