<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use app\models\DealFields;

/* @var $this yii\web\View */
/* @var $model app\models\Deal */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Deals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="deal-view box">

    <div class="box-header with-border">
        <h3></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Update']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Delete', 'data' => ['method' => 'post', 'confirm' => Yii::t('app', 'Are you sure you want to delete this?'),]]) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Back']) ?>
        </div>
    </div>
    <?php if ($model->category->parent_id == 3) { ?>
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',

                ['attribute' => 'category_id', 'value' => $model->category->title, 'label' => 'Category'],
                ['attribute' => 'discount', 'value' => function ($model) {
                    return $model->discount . '%';
                }],
                ['attribute' => 'description', 'value' => function ($model) {
                    return strip_tags(html_entity_decode($model->description));
                }],
                ['attribute' => 'terms', 'value' => function ($model) {
                    return strip_tags(html_entity_decode($model->terms));
                }],
                ['attribute' => 'hot_deal', 'value' => function ($model) {
                    if ($model->hot_deal == 1) {
                        return 'Yes';
                    } else {
                        return 'No';
                    }
                    // return strip_tags(html_entity_decode($model->terms));
                }],
                'phone',
                'email:email',
                ['attribute' => 'end_date','label' => 'Expiry Date', 'value' => function ($model) {
                    if ($model->end_date <> null) {
                        $originalDate = $model->end_date;
                        $newDate = date("d-m-Y", strtotime($originalDate));


                        return $newDate;
                    }
                }],
                'website',
                'facebook',
                'instagram',
                ['attribute' => 'imageSrc', 'format' => ['image', ['width' => '100', 'height' => '100']], 'label' => 'Image'],
                ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                    return $model->txtStatus;
                }, 'filter' => Yii::$app->params['statusArr']],
            ],
        ]) ?>
    </div>
    <?php }else if($model->category_id == 2){?>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    ['attribute' => 'rest_name', 'value' => function ($model) {
                        if ($model->category_id == 1) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 1
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }

                        } else if ($model->category_id == 2) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 3
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }
                        }
                        return $model->title;
                    }],
                    ['attribute' => 'cuisine', 'value' => function ($model) {
                        if ($model->category_id == 1) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 2
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }

                        } else if ($model->category_id == 2) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 4
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }

                        }
                        return $model->title;
                    }],
                    ['attribute' => 'hotel_id', 'value' => $model->hotel->name, 'label' => 'Hotel'],
                    ['attribute' => 'category_id', 'value' => $model->category->title, 'label' => 'Category'],
                    ['attribute' => 'sub_category', 'value' => function ($model) {
                       $sub_cat = \app\models\Category::find()->where([
                            'id' => $model->sub_category,
                        ])->one();
                       if(!empty($sub_cat) && $sub_cat <> null){
                           return $sub_cat->title;
                       }else{
                           return "-";
                       }
                    }],
                    ['attribute' => 'discount', 'value' => function ($model) {
                        return $model->discount . '%';
                    }],
                    ['attribute' => 'description', 'value' => function ($model) {
                        return strip_tags(html_entity_decode($model->description));
                    }],
                    ['attribute' => 'terms', 'value' => function ($model) {
                        return strip_tags(html_entity_decode($model->terms));
                    }],
                    ['attribute' => 'hot_deal', 'value' => function ($model) {
                        if ($model->hot_deal == 1) {
                            return 'Yes';
                        } else {
                            return 'No';
                        }
                        // return strip_tags(html_entity_decode($model->terms));
                    }],
                    'phone',
                    'email:email',
                    ['attribute' => 'end_date','label' => 'Expiry Date', 'value' => function ($model) {
                        if ($model->end_date <> null) {
                            $originalDate = $model->end_date;
                            $newDate = date("d-m-Y", strtotime($originalDate));


                            return $newDate;
                        }
                    }],
                    ['attribute' => 'website','label' => 'Website', 'format'=>'raw', 'value' => function ($model) {
                        if ($model->website <> null) {
                            return "<a href='$model->website' target='_blank'>". $model->website."</a>";
                        }
                    }],
                    ['attribute' => 'facebook','label' => 'Facebook', 'format'=>'raw', 'value' => function ($model) {
                        if ($model->facebook <> null) {
                            return "<a href='$model->facebook' target='_blank'>". $model->facebook."</a>";
                        }
                    }],
                    ['attribute' => 'instagram','label' => 'Instagram', 'format'=>'raw', 'value' => function ($model) {
                        if ($model->instagram <> null) {
                            return "<a href='$model->instagram' target='_blank'>". $model->instagram."</a>";
                        }
                    }],
                    /*'website',
                    'facebook',
                    'instagram',*/
                    ['attribute' => 'imageSrc', 'format' => ['image', ['width' => '100', 'height' => '100']], 'label' => 'Image'],
                    ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                        return $model->txtStatus;
                    }, 'filter' => Yii::$app->params['statusArr']],
                ],
            ]) ?>
        </div>
    <?php }else{ ?>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    ['attribute' => 'rest_name', 'value' => function ($model) {
                        if ($model->category_id == 1) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 1
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }

                        } else if ($model->category_id == 2) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 3
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }
                        }
                        return $model->title;
                    }],
                    ['attribute' => 'cuisine', 'value' => function ($model) {
                        if ($model->category_id == 1) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 2
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }

                        } else if ($model->category_id == 2) {
                            $rest_name = DealFields::find()->where([
                                'deal_id' => $model->id,
                                'category_filed_id' => 4
                            ])->one();
                            if (isset($rest_name->value) && $rest_name->value <> null) {
                                return $rest_name->value;
                            } else {
                                return $model->title;
                            }

                        }
                        return $model->title;
                    }],
                    ['attribute' => 'hotel_id', 'value' => $model->hotel->name, 'label' => 'Hotel'],
                    ['attribute' => 'category_id', 'value' => $model->category->title, 'label' => 'Category'],
                    ['attribute' => 'discount', 'value' => function ($model) {
                        return $model->discount . '%';
                    }],
                    ['attribute' => 'description', 'value' => function ($model) {
                        return strip_tags(html_entity_decode($model->description));
                    }],
                    ['attribute' => 'terms', 'value' => function ($model) {
                        return strip_tags(html_entity_decode($model->terms));
                    }],
                    ['attribute' => 'hot_deal', 'value' => function ($model) {
                        if ($model->hot_deal == 1) {
                            return 'Yes';
                        } else {
                            return 'No';
                        }
                        // return strip_tags(html_entity_decode($model->terms));
                    }],
                    'phone',
                    'email:email',
                    ['attribute' => 'end_date','label' => 'Expiry Date', 'value' => function ($model) {
                        if ($model->end_date <> null) {
                            $originalDate = $model->end_date;
                            $newDate = date("d-m-Y", strtotime($originalDate));


                            return $newDate;
                        }
                    }],
                    ['attribute' => 'website','label' => 'Website', 'format'=>'raw', 'value' => function ($model) {
                        if ($model->website <> null) {
                            return "<a href='$model->website' target='_blank'>". $model->website."</a>";
                        }
                    }],
                    ['attribute' => 'facebook','label' => 'Facebook', 'format'=>'raw', 'value' => function ($model) {
                        if ($model->facebook <> null) {
                            return "<a href='$model->facebook' target='_blank'>". $model->facebook."</a>";
                        }
                    }],
                    ['attribute' => 'instagram','label' => 'Instagram', 'format'=>'raw', 'value' => function ($model) {
                        if ($model->instagram <> null) {
                            return "<a href='$model->instagram' target='_blank'>". $model->instagram."</a>";
                        }
                    }],
                    /*'website',
                    'facebook',
                    'instagram',*/
                    ['attribute' => 'imageSrc', 'format' => ['image', ['width' => '100', 'height' => '100']], 'label' => 'Image'],
                    ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                        return $model->txtStatus;
                    }, 'filter' => Yii::$app->params['statusArr']],
                ],
            ]) ?>
        </div>
    <?php } ?>

</div>
