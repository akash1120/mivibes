<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\CustomGridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin">

<?php
if($searchModel!=null){
    echo ListView::widget( [
        'dataProvider' => $dataProvider,
        'viewParams' => ['id'=>$searchModel->id,'title'=>$searchModel->title,'image'=>$searchModel->image],
        'itemOptions' => ['class' => 'col-xs-12 col-sm-4'],
        'itemView' => '/deal/_item',
        'layout'=>"<div class=\"row\">{items}</div>",
        'emptyText' => '<div class="alert alert-danger">No results found!</div>'
    ] );
}
?>


        </div>
    </div>
</div>