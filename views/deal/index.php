<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\CustomGridView;
use app\models\DealFields;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .width_btn{
       /* margin-left:10px;
        margin-right:10px ;*/
        margin: 10px;
        /*clear: both;*/
    }
</style>
<style>
    input[type=checkbox] {
        display: block;
    }
</style>
<div class="deal-index box">


    <p>
        <?php
        $session = Yii::$app->session;
        if(Yii::$app->user->identity->user_type == 0 && $session->get('user_type_admin') == 3){ ?>

    <?php foreach(\app\models\Category::find()->where(['status' => 1])->all() as $key => $category) {
        if ($category->id != 3 && $category->parent_id != 3 && $category->parent_id != 2) { ?>
            <?= Html::a(Yii::t('app', 'Create ' . $category->title . ' Deal'), ['create', 'type' => $category->id], ['class' => 'btn btn-primary width_btn col-sm-3 btn-rounded']) ?>
        <?php }
    }?>
    <?php } ?>

    <!--<div class="clearfix"></div>-->
        <?php  if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) { ?>
    <div class="col-sm-12">
        <h3>Leisure Hub</h3>
    </div>

    <?php foreach(\app\models\Category::find()->where(['status' => 1])->all() as $key => $category) {
        if ($category->id != 3 && $category->parent_id == 3) { ?>
            <?= Html::a(Yii::t('app', 'Create ' . $category->title . ' Deal'), ['create', 'type' => $category->id], ['class' => 'btn btn-primary width_btn col-sm-3 btn-rounded']) ?>
        <?php }
    }?>
    <?php } ?>

    </p>
    <div class="clearfix"></div>
</div>
<?= \kartik\export\ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'exportConfig' => [
        kartik\export\ExportMenu::FORMAT_PDF => false,
        kartik\export\ExportMenu::FORMAT_HTML => false,
        kartik\export\ExportMenu::FORMAT_CSV => false,
        kartik\export\ExportMenu::FORMAT_TEXT => false,
    ],
    'columns' => [
        /* ['class' => 'yii\grid\SerialColumn'],*/

        /* 'title',*/
        ['attribute'=>'rest_name','value'=>function($model){
            if($model->category_id == 1){
                $rest_name = DealFields::find()->where([
                    'deal_id' => $model->id,
                    'category_filed_id' => 1
                ])->one();
                if(isset($rest_name->value) &&  $rest_name->value <> null){
                    return $rest_name->value;
                }else{
                    return $model->title;
                }

            }else if($model->category_id == 2){
                $rest_name = DealFields::find()->where([
                    'deal_id' => $model->id,
                    'category_filed_id' => 3
                ])->one();
                if(isset($rest_name->value) &&  $rest_name->value <> null){
                    return $rest_name->value;
                }else{
                    return $model->title;
                }
            }
            return $model->title;
        }],
        /* ['attribute'=>'hotel_id','label'=>'Hotel','value'=>function($model){
              return $model->user->name;
         }],*/
        ['attribute'=>'hotel_id','value'=>function($model){return $model->user->name;},'filter'=>ArrayHelper::map(\app\models\User::find()->where(['status'=>1,'user_type'=>0,'trashed'=>0])->orderBy('name')->asArray()->all(),'id','name')],
        ['attribute'=>'category_id','value'=>function($model){return $model->category->title;},'filter'=>ArrayHelper::map(\app\models\Category::find()->where(['status'=>1,'parent_id'=>0])->orderBy('title')->asArray()->all(),'id','title')],

        ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
        [
            'class' => 'yii\grid\ActionColumn',
            'header'=>Yii::t('app','Actions'),
            'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
            'contentOptions'=>['class'=>'noprint text-right'],
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<i class="fa fa-folder"></i>', ['view','id'=>$model->id], [
                        'title' => Yii::t('app', 'View'),
                        'data-toggle'=>'tooltip',
                        'class'=>'btn btn-info btn-xs',
                    ]);
                },
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-edit"></i>', ['update','id'=>$model->id], [
                        'title' => Yii::t('app', 'Update'),
                        'data-toggle'=>'tooltip',
                        'class'=>'btn btn-success btn-xs',
                    ]);
                },
                'delete' => function ($url, $model) {
                    if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
                        return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle' => 'tooltip',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this?'),
                            'class' => 'btn btn-danger btn-xs',
                        ]);
                    }
                },
            ],
        ],
    ],
]);
?>
    <div class="deal-index box">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           /* ['class' => 'yii\grid\SerialColumn'],*/

           /* 'title',*/
            ['attribute'=>'rest_name','label'=>'Rest Name','value'=>function($model){
                if($model->category_id == 1){
                    $rest_name = DealFields::find()->where([
                        'deal_id' => $model->id,
                        'category_filed_id' => 1
                    ])->one();
                    if(isset($rest_name->value) &&  $rest_name->value <> null){
                        return $rest_name->value;
                    }else{
                        return $model->title;
                    }

                }else if($model->category_id == 2){
                    $rest_name = DealFields::find()->where([
                        'deal_id' => $model->id,
                        'category_filed_id' => 3
                    ])->one();
                    if(isset($rest_name->value) &&  $rest_name->value <> null){
                        return $rest_name->value;
                    }else{
                        return $model->title;
                    }
                }
                return $model->title;
            }],
          /* ['attribute'=>'hotel_id','label'=>'Hotel','value'=>function($model){
                return $model->user->name;
           }],*/
            ['attribute'=>'hotel_id','label'=>'Hotel','value'=>function($model){return $model->user->name;},'filter'=>ArrayHelper::map(\app\models\User::find()->where(['status'=>1,'user_type'=>0,'trashed'=>0])->orderBy('name')->asArray()->all(),'id','name')],
            ['attribute'=>'category_id','label'=>'Category','value'=>function($model){return $model->category->title;},'filter'=>ArrayHelper::map(\app\models\Category::find()->where(['status'=>1])->andWhere(['!=','parent_id','2'])->orderBy('title')->asArray()->all(),'id','title')],

            ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>Yii::t('app','Actions'),
                'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                'contentOptions'=>['class'=>'noprint text-right'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', ['view','id'=>$model->id], [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', ['update','id'=>$model->id], [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
    if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) {
        return Html::a('<i class="fa fa-remove"></i>', $url, [
            'title' => Yii::t('app', 'Delete'),
            'data-toggle' => 'tooltip',
            'data-method' => 'post',
            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this?'),
            'class' => 'btn btn-danger btn-xs',
        ]);
    }
                    },
                ],
            ],
        ],
    ]); ?>


</div>
