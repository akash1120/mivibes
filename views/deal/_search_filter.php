<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\models\City;
use yii\helpers\Url;
use app\assetfiles\DatePickerAsset;

DatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

    <style>
        .filter select {
            height: auto !important;
        }
    </style>
    <!--<script>
        $( function() {
            $( "#slider-range" ).slider({
                range: true,
                min: 0,
                max: 500,
                values: [ 0, 500 ],
                slide: function( event, ui ) {
                    $( "#amount" ).val( "AED " + ui.values[ 0 ] + " - AED " + ui.values[ 1 ] );
                }
            });
            $( "#amount" ).val( "AED " + $( "#slider-range" ).slider( "values", 0 ) +
                " - AED " + $( "#slider-range" ).slider( "values", 1 ) );
        } );
    </script>-->
    <style>
        .filter select {
            height: auto !important;

        }

        .select2-container {
            margin-bottom: 20px !important;
        }

        .select2-selection__arrow {
            display: none !important;
        }

        .select2-selection__placeholder {
            /*font-weight: 800;*/
            padding-left: 10px;

        }

        .select2-container--krajee .select2-selection--single .select2-selection__clear {
            right: 1rem !important;
        }

        #select2-dealsearch-area_id-container, #select2-deal-cities-container {

            padding-top: 5px;
        }

        ::-webkit-input-placeholder { /* Edge */
            /* font-weight: 800;*/
            color: #999999 !important;
            padding-left: 5px !important;
            font-size: 14px;
        }

        :-ms-input-placeholder { /* Internet Explorer */
            /* font-weight: 800;*/
            color: #999999 !important;
            padding-left: 5px !important;
            font-size: 14px;
        }

        ::placeholder {
            /* font-weight: 800;*/
            color: #999999 !important;
            padding-left: 5px !important;
            font-size: 14px;
        }

        .select2-container--krajee .select2-selection--single {
            height: 40px;
            padding: 12px 24px 6px 5px;
            /* border: none!important;
             border-bottom: 2px solid #c8ccd4 !important;
             box-shadow: none !important;
             border-radius: 0px !important;*/
        }

        .select2-selection--single {
            border-radius: 5px !important;
            height: 38px !important;
        }

        .select2-container--default {
            width: 100% !important;
        }
    </style>
    <div class="category-search">

<?php

$current_catgory = \app\models\Category::find()->where(['id' => $category_id])->one();
?>


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin"><img
                        src="images/avatar/<?= $current_catgory->image; ?>" class="img-responsive center-block"/></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin">
                <?php $form = ActiveForm::begin([
                    'action' => ['filter-results'],
                    'method' => 'get',
                    'options' => [
                        'class' => 'filter'
                    ]
                ]); ?>
                <?php if ($category_id != 3) { ?>

                    <?php if ($category_id != 2) { ?>
                        <?= \kartik\select2\Select2::widget([
                            'model' => $model,
                            'attribute' => 'hotel_id',
                            'data' => \yii\helpers\ArrayHelper::map(User::find()->where(['status' => 1, 'user_type' => 0])->orderBy('name')->all(), 'id', function ($model) {
                                //return $model->first_name . " " . $model->last_name;
                                return $model->name;
                            }),
                            'options' => ['placeholder' => 'By Hotel ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    <?php } ?>
                    <?php if ($category_id == 2) { ?>
                        <?= \kartik\select2\Select2::widget([
                            'model' => $model,
                            'attribute' => 'sub_category',
                            'data' => \yii\helpers\ArrayHelper::map(\app\models\Category::find()->where(['status' => 1, 'parent_id' => 2])->orderBy('title')->all(), 'id', function ($model) {
                                //return $model->first_name . " " . $model->last_name;
                                return $model->title;
                            }),
                            'options' => ['placeholder' => 'By Category ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    <?php } ?>
                <?php } else { ?>

                    <?= \kartik\select2\Select2::widget([
                        'model' => $model,
                        'attribute' => 'subcategory',
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Category::find()->where(['status' => 1, 'parent_id' => 3])->orderBy('title')->all(), 'id', function ($model) {
                            //return $model->first_name . " " . $model->last_name;
                            return $model->title;
                        }),
                        'options' => ['placeholder' => 'By Category ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                <?php } ?>


                <?php if ($category_id != 3) { ?>
                    <?php if ($category_id != 2) { ?>
                        <?= $form->field($model, 'cuisine')->textInput(['maxlength' => true, 'placeholder' => 'By Cuisine'])->label(false); ?>
                    <?php } ?>
                    <?= \kartik\select2\Select2::widget([
                        'model' => $model,
                        'attribute' => 'city_id',
                        'data' => \yii\helpers\ArrayHelper::map(City::find()->where(['status' => 1, 'trashed' => 0])->orderBy('title')->all(), 'id', function ($model) {
                            //return $model->first_name . " " . $model->last_name;
                            return $model->title;
                        }),
                        'options' => ['placeholder' => 'By City ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                    <?php if ($category_id != 2) { ?>
                        <select id="dealsearch-area_id" class="form-control" name="DealSearch[area_id]"
                                aria-invalid="false">
                            <option value=''>By Area</option>
                        </select>
                    <?php } ?>

                <?php } else { ?>
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'By Title'])->label(false); ?>

                    <select id="deal-cities" class="form-control" name="DealSearch[cities]" aria-invalid="false">
                        <option value="">By City</option>
                        <option value="uae" <?php if (isset($model->cities) && $model->cities == 'uae') {
                            echo "selected";
                        } ?>>All Over UAE
                        </option>
                        <option value="dubai" <?php if (isset($model->cities) && $model->cities == 'dubai') {
                            echo "selected";
                        } ?>>Dubai
                        </option>
                        <option value="abu dabhi" <?php if (isset($model->cities) && $model->cities == 'abu dabhi') {
                            echo "selected";
                        } ?>>Abu Dhabi
                        </option>
                        <option value="sharjah" <?php if (isset($model->cities) && $model->cities == 'sharjah') {
                            echo "selected";
                        } ?>>Sharjah
                        </option>
                        <option value="al ain" <?php if (isset($model->cities) && $model->cities == 'al ain') {
                            echo "selected";
                        } ?>>Al Ain
                        </option>
                        <option value="ajman" <?php if (isset($model->cities) && $model->cities == 'ajman') {
                            echo "selected";
                        } ?>>Ajman
                        </option>
                        <option value="ras al khaimah" <?php if (isset($model->cities) && $model->cities == 'ras al khaimah') {
                            echo "selected";
                        } ?>>Ras Al Khaimah
                        </option>
                        <option value="umm al quwain" <?php if (isset($model->cities) && $model->cities == 'umm al quwain') {
                            echo "selected";
                        } ?>>Umm Al Quwain
                        </option>
                    </select>


                <?php } ?>
                <?= $form->field($model, 'category_id')->hiddenInput(['value' => $category_id])->label(false); ?>
                <!--  <p class="price">
                      <label for="amount">Price range:</label>
                  </p>
                  <div id="slider-range"></div>
                  <input type="text" id="amount" readonly style="border:0; color:#333333; font-weight:noraml;">-->
                <input type="submit" name="" value="Filter">

                <span class="showall">
                            <a href="<?= Yii::$app->params['siteUrl'] . 'deal/filter-results?type=' . $category_id; ?>">
                            Show All
                                 </a>
                        </span>


                <?php ActiveForm::end(); ?>
            </div>
        </div>


        <?php if ($category_id == 3) { ?>
            <?= $this->registerJs('$("#deal-cities").select2();'); ?>
        <?php } else { ?>
            <?= $this->registerJs('$("#dealsearch-area_id").select2();'); ?>
        <?php } ?>

    </div>
<?= $this->registerJs('

$("#dealsearch-area_id").select2();

        $("body").on("change", "#dealsearch-city_id", function(e) {
            var city_id = $(this).val();
            $.ajax({
                url:  "' . Url::base(true) . '/hotel/getareasofcity?id="+city_id,
                type: "GET",
                success : function(res){
                     var obj = JSON.parse(res);
                      if(obj.options) {
                        $("#dealsearch-area_id").html(obj.options);
                        $("#dealsearch-area_id").select2();
                    }
                     

                },
                error : function(data){
                    console.log(data);
                }
            });
        });
'); ?>