<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assetfiles\AppAsset;
use app\models\Announcement;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
$this->registerJs('
	$(".sidebar-menu").tree();
');
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(Yii::$app->params['siteName'] . ' - ' . $this->title) ?></title>
        <base href="<?= Yii::$app->params['siteUrl'] ?>"/>
        <?php $this->head() ?>
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
      <!--  <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                    appId: "e1f51b59-270a-4e39-aa36-db5d790478de",
                });
            });
        </script>-->
    </head>
    <body class="sidebar-mini skin-black-light">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?= Yii::$app->params['siteUrl'] ?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>E</b>B</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img style="height: 50px" src="<?= Yii::$app->params['siteUrl'] ?>/images/logo.jpg"
                                           class="img-responsive"/></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Yii::$app->params['siteUrl'] . Yii::$app->user->identity->imageSrc ?>"
                                     class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= Yii::$app->params['siteUrl'] . Yii::$app->user->identity->imageSrc ?>"
                                         class="img-circle" alt="User Image">
                                    <p>
                                        <?= Yii::$app->user->identity->username ?>
                                        <small>Member
                                            since <?= Yii::$app->formatter->asDate(Yii::$app->user->identity->created_at, "php:M. Y") ?></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <!--a href="#" class="btn btn-default btn-flat">Profile</a-->
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a('Sign out', ['site/logout-admin'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <?php

        ?>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu tree" data-widget="tree">
<?php if (Yii::$app->user->identity->user_type != 1 && Yii::$app->user->identity->user_type != 2) { ?>

                    <li class="<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index-hotelsettings') echo 'active'; ?>"><?= Html::a('<i class="fa fa-dashboard"></i> <span>Dashboard</span>', ['site/index-hotelsettings']) ?></li>
                    <li class="<?php if (Yii::$app->controller->id == 'hotel') echo 'active'; ?>"><?= Html::a('<i class="fa fa-list"></i> Manage Hotel Settings', ['hotel/update?id='.Yii::$app->user->identity->id]) ?></li>
                    <li class="<?php if (Yii::$app->controller->id == 'deal') echo 'active'; ?>"><?= Html::a('<i class="fa fa-list"></i> Manage Deals', ['deal/index']) ?></li>
    <!--<li class="<?php /*if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') echo 'active'; */?>"><?/*= Html::a('<i class="fa fa-sign-out"></i> <span>Back to Home</span>', ['site/index']) */?></li>-->
                 <?php } ?>
                    <li><?= Html::a('<i class="fa fa-sign-out"></i> Logout', ['site/logout-admin'], ['data-method' => 'post']) ?></li>
                   <!-- <li><?/*= Html::a('<i class="fa fa-sign-out"></i> Logout', ['site/logout'], ['data-method' => 'post']) */?></li>-->
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper" style="background-color: #ECF0F5 !important;">
            <?php
            $announcements = Announcement::find()->where(['and', ['<=', 'DATE(start_date)', date("Y-m-d")], ['>=', 'DATE(end_date)', date("Y-m-d")], ['trashed' => 0]])->asArray()->all();
            if ($announcements != null) {
                foreach ($announcements as $announcement) {
                    echo '
			<div style="padding: 15px;margin: 0;padding-bottom: 0;">
				<div class="callout callout-' . ($announcement['color_class'] != null && $announcement['color_class'] != '' ? $announcement['color_class'] : 'info') . '" style="margin: 0;">
					<i class="fa fa-bullhorn"></i>&nbsp;&nbsp;' . $announcement['heading'] . '<br />' . nl2br($announcement['descp']) . '
				</div>
			</div>';
                }
            }
            ?>
            <?php
            /* Yii::$app->user->identity->noticeText; */
            ?>
            <section class="content-header">
                <h1><?= $this->title . (isset($this->subHeading) ? '<small>' . $this->subHeading . '</small>' : '') ?></h1>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>
            <section class="content">
                <?= Alert::widget() ?>
                <?= $content ?>
            </section>
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; <?= date("Y") . ' ' . Yii::$app->params['siteName'] ?>.</strong> All rights
            reserved.
        </footer>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true, 'tabindex' => false],
    'options' => ['tabindex' => false]
]);
echo "<div id='modalContent'><div style='text-align:center'><img src='" . Yii::getAlias('@web') . '/loader_gif.gif' . "'></div></div>";
yii\bootstrap\Modal::end();
?>