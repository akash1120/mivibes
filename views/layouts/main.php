<?php

/* @var $this \yii\web\View */

/* @var $content string */
use app\assetfiles\AppAsset;
use app\models\Announcement;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use AddToHomescreen\AddToHomescreenWidget;

AppAsset::register($this);
$this->registerJs('
	$(".sidebar-menu").tree();
');
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="shortcut icon" sizes="16x16" href="<?= Yii::$app->params['siteUrl'] . 'images/icon-16x16.png'; ?>">
        <link rel="shortcut icon" sizes="196x196" href="<?= Yii::$app->params['siteUrl'] . 'images/icon-196x196.png'; ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(Yii::$app->params['siteName'] . ' - ' . $this->title) ?></title>
        <base href="<?= Yii::$app->params['siteUrl'] ?>"/>
        <?php $this->head() ?>
         <?= AddToHomescreenWidget::widget() ;?>
       <!-- <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                    appId: "e1f51b59-270a-4e39-aa36-db5d790478de",
                    safari_web_id: "web.onesignal.auto.2d9123a5-f6c1-46fe-a6d4-d9acca",
                });
            });
        </script>-->

    </head>
    <body class="sidebar-mini skin-black-light">
    <?php $this->beginBody() ?>


    <!-- top header -->
    <div class="top-wrapper">

        <?php if (Yii::$app->controller->id != 'site') { ?>

            <!-- <a href="<? /*= Yii::$app->params['siteUrl']; */ ?>"><i class="fa fa-chevron-left" style="color: white"></i></a>-->
        <?php } ?>
        <?php if (Yii::$app->controller->id == 'city') { ?>
            <?php /*if (isset(Yii::$app->controller->action->id) && Yii::$app->controller->action->id != "index") { */?><!--
                <a href="<?/*= Yii::$app->params['siteUrl'] . 'city'; */?>"><i class="fa fa-chevron-left"
                                                                           style="color: white"></i></a>
            <?php /*} else if (Yii::$app->controller->action->id == 'index') { */?>
                <a href="<?/*= Yii::$app->params['siteUrl']; */?>"><i class="fa fa-chevron-left"
                                                                  style="color: white"></i></a>
            --><?php /*} */?>
            <a onclick="goBack()"><i class="fa fa-chevron-left" style="color: white"></i></a>
            City
        <?php } else if (Yii::$app->controller->id == 'area') { ?>

           <!-- <?php /*if (isset(Yii::$app->controller->action->id) && Yii::$app->controller->action->id != "index") { */?>
                <a href="<?/*= Yii::$app->params['siteUrl'] . 'area'; */?>"><i class="fa fa-chevron-left"
                                                                           style="color: white"></i></a>
            <?php /*} else if (Yii::$app->controller->action->id == 'index') { */?>
                <a href="<?/*= Yii::$app->params['siteUrl']; */?>"><i class="fa fa-chevron-left"
                                                                  style="color: white"></i></a>
            --><?php /*} */?>
            <a onclick="goBack()"><i class="fa fa-chevron-left" style="color: white"></i></a>
            Area
        <?php } else if (Yii::$app->controller->id == 'category') { ?>

           <!-- <?php /*if (isset(Yii::$app->controller->action->id) && Yii::$app->controller->action->id != "index") { */?>
                <a href="<?/*= Yii::$app->params['siteUrl'] . 'category'; */?>"><i class="fa fa-chevron-left"
                                                                               style="color: white"></i></a>
            <?php /*} else if (Yii::$app->controller->action->id == 'index') { */?>
                <a href="<?/*= Yii::$app->params['siteUrl']; */?>"><i class="fa fa-chevron-left"
                                                                  style="color: white"></i></a>
            --><?php /*} */?>
            <a onclick="goBack()"><i class="fa fa-chevron-left" style="color: white"></i></a>
            Marriot Dinning
        <?php } else if (Yii::$app->controller->id == 'deal') { ?>
           <!-- <?php /*if (Yii::$app->controller->action->id == 'search-results') { */?>
                <a href="<?/*= Yii::$app->params['siteUrl'] . 'deal/searchall'; */?>"><i class="fa fa-chevron-left"
                                                                                          style="color: white"></i></a>

            <?php /*} else if (Yii::$app->controller->action->id == 'filter-results') { */?>
                <a href="<?/*= Yii::$app->params['siteUrl'] . 'deal/filter?id=1'; */?>"><i class="fa fa-chevron-left"
                                                                                  style="color: white"></i></a>

            <?php /*} else if (Yii::$app->controller->action->id == 'viewdeal' || Yii::$app->controller->action->id == 'filter' || Yii::$app->controller->action->id == 'searchall') { */?>
                <a href="<?/*= Yii::$app->params['siteUrl']; */?>"><i class="fa fa-chevron-left"
                                                                                  style="color: white"></i></a>

            <?php /*} else if (isset(Yii::$app->controller->action->id) && Yii::$app->controller->action->id != "index") { */?>
                <a href="<?/*= Yii::$app->params['siteUrl'] . 'deal'; */?>"><i class="fa fa-chevron-left"
                                                                           style="color: white"></i></a>
            <?php /*} else { */?>
                <a href="<?/*= Yii::$app->params['siteUrl']; */?>"><i class="fa fa-chevron-left"
                                                                           style="color: white"></i></a>
            --><?php /*} */?>
            <a onclick="goBack()"><i class="fa fa-chevron-left" style="color: white"></i></a>
            Deals
        <?php } else if (Yii::$app->controller->id == 'hotel') { ?>
           <!-- <?php /*if (isset(Yii::$app->controller->action->id) && Yii::$app->controller->action->id != "index") { */?>
                <a href="<?/*= Yii::$app->params['siteUrl'] . 'hotel'; */?>"><i class="fa fa-chevron-left"
                                                                            style="color: white"></i></a>
            <?php /*} else if (Yii::$app->controller->action->id == 'index') { */?>
                <a href="<?/*= Yii::$app->params['siteUrl']; */?>"><i class="fa fa-chevron-left"
                                                                  style="color: white"></i></a>
            --><?php /*} */?>
            <a onclick="goBack()"><i class="fa fa-chevron-left" style="color: white"></i></a>
            Hotel
        <?php } else { ?>
            <a style="color: #ffffff" href="<?= Yii::$app->params['siteUrl']; ?>">Home</a>
        <?php } ?>
        <div class="header"></div>
        <input type="checkbox" class="openSidebarMenu" id="openSidebarMenu">
        <label for="openSidebarMenu" class="sidebarIconToggle">
            <div class="spinner diagonal part-1"></div>
            <div class="spinner horizontal"></div>
            <div class="spinner diagonal part-2"></div>
        </label>
        <div id="sidebarMenu">
            <ul class="sidebarMenuInner">
                <li><?= Html::a(' Home', ['site/index']) ?></li>
                <?php if (Yii::$app->user->identity->user_type == 1 || Yii::$app->user->identity->user_type == 2) { ?>
                    <li><?= Html::a(' Manage Hotels', ['hotel/index']) ?></li>
                    <li><?= Html::a(' Manage Category', ['category/index']) ?></li>
                    <li><?= Html::a(' Manage Deals', ['deal/index']) ?></li>
                    <li><?= Html::a(' Manage Cities', ['city/index']) ?></li>
                    <li><?= Html::a(' Manage Area', ['area/index']) ?></li>
                <?php } ?>
               <!-- <li><?/*= Html::a(' Dashboard', ['site/index-hotelsettings']) */?></li>-->
                <li><?= Html::a(' Marriott Dining', ['deal/filter?id=1']) ?></li>
                <li><?= Html::a(' Mi Hot Deals', ['deal/filter?id=2']) ?></li>
                <li><?= Html::a(' Leisure Hub', ['deal/filter?id=3']) ?></li>

               <!-- <li><?/*= Html::a(' Category', ['site/index']) */?></li>
                <li><?/*= Html::a(' Deals', ['deal/search-results']) */?></li>-->
                <!--<li><? /*= Html::a(' Manage Announcements', ['announcement/index']) */ ?></li>-->
                <!-- <li><? /*= Html::a(' Manage Staff', ['staff/index']) */ ?></li>-->

                <!-- <li><? /*= Html::a(' Terms and Conditions', ['terms-and-conditions/index']) */ ?></li>-->
                <li><?= Html::a(' Logout', ['site/logout'], ['data-method' => 'post']) ?></li>

            </ul>
        </div>
    </div>
    <!-- top header -->


    <div class="wrapper">

        <div class="content-wrapper" style="margin-left: 0;">
            <?php
            $announcements = Announcement::find()->where(['and', ['<=', 'DATE(start_date)', date("Y-m-d")], ['>=', 'DATE(end_date)', date("Y-m-d")], ['trashed' => 0]])->asArray()->all();
            if ($announcements != null) {
                foreach ($announcements as $announcement) {
                    echo '
			<div style="padding: 15px;margin: 0;padding-bottom: 0;">
				<div class="callout callout-' . ($announcement['color_class'] != null && $announcement['color_class'] != '' ? $announcement['color_class'] : 'info') . '" style="margin: 0;">
					<i class="fa fa-bullhorn"></i>&nbsp;&nbsp;' . $announcement['heading'] . '<br />' . nl2br($announcement['descp']) . '
				</div>
			</div>';
                }
            }
            ?>
            <?php
            /* Yii::$app->user->identity->noticeText; */
            ?>
            <!--  <section class="content-header">
                <? /*= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) */ ?>
            </section>-->
            <section class="content">
                <?= Alert::widget() ?>
                <?= $content ?>
            </section>
        </div>
        <footer class="main-footer" style="margin-left: 0;">
            <ul>
                <li><a href="<?= Yii::$app->params['siteUrl']; ?>"><img
                                src="<?= Yii::$app->params['siteUrl'] . 'images/home-icon.jpg'; ?>"
                                class="img-responsive center-block">HOME</a></li>
                <!--<li><a href="#"><img src="<?/*= Yii::$app->params['siteUrl'] . 'images/start-icon.jpg'; */?>"
                                     class="img-responsive center-block">favorites</a></li>-->
                <li><a href="<?= Yii::$app->params['siteUrl'] . 'deal/searchall'; ?>"><img
                                src="<?= Yii::$app->params['siteUrl'] . 'images/search-icon.jpg'; ?>"
                                class="img-responsive center-block">Search</a></li>
            </ul>
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; <?= date("Y") . ' ' . Yii::$app->params['siteName'] ?>.</strong> All rights
            reserved.
        </footer>
    </div>
    <?php


    $this->endBody();
    ?>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    </body>
    </html>
<?php $this->endPage() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true, 'tabindex' => false],
    'options' => ['tabindex' => false]
]);
echo "<div id='modalContent'><div style='text-align:center'><img src='" . Yii::getAlias('@web') . '/loader_gif.gif' . "'></div></div>";
yii\bootstrap\Modal::end();
?>