<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assetfiles\LoginAsset;
use AddToHomescreen\AddToHomescreenWidget;

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" sizes="16x16" href="<?= Yii::$app->params['siteUrl'] . 'images/icon-16x16.png'; ?>">
    <link rel="shortcut icon" sizes="196x196" href="<?= Yii::$app->params['siteUrl'] . 'images/icon-196x196.png'; ?>">
    <base href="<?= Yii::$app->params['siteUrl']?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::$app->params['siteName'].' - '.$this->title) ?></title>
    <?php $this->head() ?>
    <?= AddToHomescreenWidget::widget() ;?>
</head>

<body class="">
<?php $this->beginBody() ?>
<div class="login-box-body"><?= $content;?></div>
<!--<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-top"><a href="#"><img src="<?/*= Yii::$app->params['siteUrl'].'/images/black-logo.jpg';*/?>" class="img-responsive center-block" /></a></div>
        <div class="col-lg-12 margin-top">
            <form id="" action="">
                <label for="inp" class="inp">
                    <input type="text" id="inp" placeholder="&nbsp;">
                    <span class="label">Hotel</span>
                    <span class="border"></span>
                </label>
                <label for="pass" class="inp">
                    <input type="password" id="inp" placeholder="&nbsp;">
                    <span class="label">Password</span>
                    <span class="border"></span>
                </label>
                <div class="col-lg-12 col-md-12 margin">
                    <input class="magic-checkbox" type="checkbox" name="layout" id="1" value="option">
                    <label for="1"></label>
                    <label class="pull-left text" for="1">
                        Remember Password.
                    </label>

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <input type="submit" name="" value="login">
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>-->
<?php $this->endBody() ?>
</body>


<!--<body class="hold-transition login-page">
<?php /*$this->beginBody() */?>
<div class="login-box">
  <div class="login-logo">
    <?/*= Html::a('<img src="'.Yii::$app->params['siteUrl'].'/images/black-logo.jpg">',['site/login'])*/?>
  </div>
  <div class="login-box-body"><?/*= $content*/?></div>
</div>
<?php /*$this->endBody() */?>
</body>-->
</html>
<?php $this->endPage() ?>
