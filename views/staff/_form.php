<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;
use yii\web\JqueryAsset;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile('@web/'.'plugins/datepicker/datepicker3.css', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/'.'plugins/datepicker/bootstrap-datepicker.js', ['depends' => [JqueryAsset::className()]]);

$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		endDate: new Date(),
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
');

?>
<div class="user-form box">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')) ?></div>
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'date_of_birth')->textInput(['class'=>'form-control dtpicker']) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'registration_no')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="row">
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?></div>
        </div>
        <div class="row">
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'new_password')->textInput() ?></div>
        	<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?></div>
        </div>
        <?php if($model->imageSrc!=null && $model->imageSrc!=Yii::$app->params['default_avatar']){?>
        <div class="form-group">
            <label class="control-label"><?= Yii::t('app','Old Image')?></label>
            <div><img src="<?= $model->imageSrc?>" width="128" /></div>
        </div>
        <?php }?>
        <?= $form->field($model, 'image')->fileInput() ?>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'notice')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'notice_class')->dropDownList(Yii::$app->params['noticeClass']) ?></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'vat_number')->textInput(['maxlength' => true]) ?></div>
            <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'address')->textarea(['rows' => 2]); ?></div>
        </div>
            <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
