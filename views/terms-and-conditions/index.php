<?php

use yii\helpers\Html;
use app\widgets\CustomGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TermsAndConditionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Terms And Conditions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terms-and-conditions-index">

    <?= CustomGridView::widget([
		'create'=>false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],

            'terms:ntext',
            'client_notes:ntext',
            'admin_notes:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>Yii::t('app','Actions'),
                'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                'contentOptions'=>['class'=>'noprint text-right'],
                'template' => '{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                    /*'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle'=>'tooltip',
                            'data-method'=>'post',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                            'class'=>'btn btn-danger btn-xs',
                        ]);
                    },*/
                ],
            ],
        ],
    ]); ?>
</div>
