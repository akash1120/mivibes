<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TermsAndConditions */

$this->title = 'Update Terms And Conditions: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Terms And Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="terms-and-conditions-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
