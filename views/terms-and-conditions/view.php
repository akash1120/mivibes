<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TermsAndConditions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Terms And Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terms-and-conditions-view box">
    <div class="box-header with-border">
        <h3></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Update']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Delete', 'data'=>['method' => 'post', 'confirm' => 'Are you sure you want to delete this?',]]) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Back']) ?>
        </div>
    </div>
    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
	            'terms:ntext',
            'client_notes:ntext',
            'admin_notes:ntext',
            ],
        ]) ?>
    </div>
</div>
