<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TermsAndConditions */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="terms-and-conditions-form box">
	<?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
	    <?= $form->field($model, 'terms')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'client_notes')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'admin_notes')->textarea(['rows' => 6]) ?>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
