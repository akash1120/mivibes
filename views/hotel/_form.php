<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\City;
use yii\helpers\Url;
use app\assetfiles\DatePickerAsset;

DatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		endDate: new Date(),
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
');

?>


<div class="user-form">
    <div class="box box-success">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="box-body">

            <div class="row">
                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?></div>
                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'new_password')->textInput(['value' => $model->user_password]) ?></div>


                    <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'admin_password')->textInput() ?></div>



                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?></div>
                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(City::find()->where(['status' => 1, 'trashed' => 0])->orderBy('title')->asArray()->all(), 'id', 'title')) ?></div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group field-user-area_id required has-success">
                        <label class="control-label" for="user-area_id">Area</label>
                        <select id="user-area_ids" class="form-control" name="User[area_id]" aria-required="true"
                                aria-invalid="false">
                            <option value="">Select area</option>
                        </select>

                        <div class="help-block"></div>
                    </div>
                    <input type="hidden" value="<?= $model->area_id; ?>" id="area_id_value">
                </div>
                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusArr']) ?></div>
            </div>
            <?php if ($model->imageSrc != null && $model->imageSrc != Yii::$app->params['default_avatar']) { ?>
                <div class="form-group">
                    <label class="control-label"><?= Yii::t('app', 'Old Image') ?></label>
                    <div><img src="<?= $model->imageSrc ?>" width="128"/></div>
                </div>
            <?php } ?>
            <?= $form->field($model, 'image')->fileInput() ?>

            <div class="row">
                <div class="col-xs-12 col-sm-6"><?= $form->field($model, 'address')->textarea(['rows' => 2]); ?></div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


    <?= $this->registerJs('


        var saved_area = $("#area_id_value").val();
        var current_city = $("#user-city_id").val();


        $.ajax({
            url:  "' . Url::base(true) . '/hotel/getareasofcity?id="+current_city+"&areaid="+saved_area,
            type: "GET",
            success : function(res){
                var obj = JSON.parse(res);
                $("#user-area_ids").html(obj.options);

            },
            error : function(data){
                console.log(data);
            }
        });




        $("body").on("change", "#user-city_id", function(e) {
            var city_id = $(this).val();
            $.ajax({
                url: "' . Url::base(true) . '/hotel/getareasofcity?id="+city_id,
                type: "GET",
                success : function(res){
                     var obj = JSON.parse(res);
                     $("#user-area_ids").html(obj.options);

                },
                error : function(data){
                    console.log(data);
                }
            });
        });
'); ?>
