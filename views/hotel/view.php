<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use app\widgets\CustomGridView;
use app\models\DealFields;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="user-view box">
    <div class="box-header with-border">
        <h3></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Update']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Delete', 'data' => ['method' => 'post', 'confirm' => Yii::t('app', 'Are you sure you want to delete this?'),]]) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle' => 'tooltip', 'title' => 'Back']) ?>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        ['attribute' => 'imageSrc', 'format' => ['image', ['width' => '100', 'height' => '100']]],
                        'name',
                        'username',
                        ['attribute' => 'city_id', 'value' => $model->city->title],
                        'email:email',
                        'mobile',
                        ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                            return $model->txtStatus;
                        }, 'filter' => Yii::$app->params['statusArr']],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="deal-index box">
        <div class="row">
        <div class="col-sm-12" >
            <h3 style="padding-left: 15px;"> Related Deals</h3>
        </div>
        </div>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= CustomGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                /* ['class' => 'yii\grid\SerialColumn'],*/

                /* 'title',*/
                ['attribute'=>'rest_name','value'=>function($model){
                    if($model->category_id == 1){
                        $rest_name = DealFields::find()->where([
                            'deal_id' => $model->id,
                            'category_filed_id' => 1
                        ])->one();
                        if(isset($rest_name->value) &&  $rest_name->value <> null){
                            return $rest_name->value;
                        }else{
                            return $model->title;
                        }

                    }else if($model->category_id == 2){
                        $rest_name = DealFields::find()->where([
                            'deal_id' => $model->id,
                            'category_filed_id' => 3
                        ])->one();
                        if(isset($rest_name->value) &&  $rest_name->value <> null){
                            return $rest_name->value;
                        }else{
                            return $model->title;
                        }
                    }
                    return $model->name;
                }],
                /* ['attribute'=>'hotel_id','label'=>'Hotel','value'=>function($model){
                     return $model->user->name;
                 }],*/
                ['attribute' => 'category_id', 'label' => 'Category', 'value' => function ($model) {
                    return $model->category->title;
                }],

                ['format' => 'html', 'attribute' => 'status', 'value' => function ($model) {
                    return $model->txtStatus;
                }, 'filter' => Yii::$app->params['statusArr']],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => Yii::t('app', 'Actions'),
                    'headerOptions' => ['class' => 'noprint', 'style' => 'width:100px;'],
                    'contentOptions' => ['class' => 'noprint text-right'],
                    'template' => '{view} {update}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<i class="fa fa-folder"></i>', ['deal/view', 'id' => $model->id], [
                                'title' => Yii::t('app', 'View'),
                                'data-toggle' => 'tooltip',
                                'class' => 'btn btn-info btn-xs',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-edit"></i>', ['deal/update', 'id' => $model->id], [
                                'title' => Yii::t('app', 'Update'),
                                'data-toggle' => 'tooltip',
                                'class' => 'btn btn-success btn-xs',
                            ]);
                        },
                        /* 'delete' => function ($url, $model) {
                             return Html::a('<i class="fa fa-remove"></i>', $url, [
                                 'title' => Yii::t('app', 'Delete'),
                                 'data-toggle'=>'tooltip',
                                 'data-method'=>'post',
                                 'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                                 'class'=>'btn btn-danger btn-xs',
                             ]);
                         },*/
                    ],
                ],
            ],
        ]); ?>


    </div>
</div>
</div>

<!--<div class="box box-warning">
    <div class="box-header">
        <div class="note-form">
            <?php /*$form = ActiveForm::begin(); */ ?>
			<? /*= $form->field($modelNote, 'comment',['template'=>'
            <div class="input-group">
                <span class="input-group-addon">Note</span>
                {input}
                <span class="input-group-btn">
                    '.Html::submitButton(Yii::t('app', 'Post'), ['class' => 'btn btn-warning']).'
                </span>
            </div>
            {error}
            '])->textInput(['maxlength' => true]) */ ?>
            <?php /*ActiveForm::end(); */ ?>
        </div>
    </div>
	<?php
/*    echo ListView::widget( [
        'dataProvider' => $dataProvider,
        'id' => 'my-listview-id',
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_note_item',
		'emptyText'=>'',
        'layout'=>"
            <div class=\"box-body chat\">{items}</div>
            <div class=\"box-footer\"><center>{pager}</center></div>
        ",
    ] );
    */ ?>
</div>-->
