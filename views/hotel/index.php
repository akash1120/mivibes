<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\widgets\CustomGridView;
use app\models\City;
use app\models\Package;
use app\models\Contracts;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Hotels');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    input[type=checkbox] {
        display: block;
    }
</style>
<div class="user-index">
    <?= \kartik\export\ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'exportConfig' => [
            kartik\export\ExportMenu::FORMAT_PDF => false,
            kartik\export\ExportMenu::FORMAT_HTML => false,
            kartik\export\ExportMenu::FORMAT_CSV => false,
            kartik\export\ExportMenu::FORMAT_TEXT => false,
        ],
        'columns' => [
            //  ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],
            'id',
            'name',
            ['attribute'=>'username','value'=>function($model)

            {
                $result = \app\models\Deal::find()->where(['hotel_id' => $model->id])->asArray()->all();
                return count($result);


            },'label'=>'No. of Deals'],
            /*            'email:email',
                        'mobile',
                        ['attribute'=>'city_id','value'=>function($model){return $model->city->title;},'filter'=>ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')],*/

            ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
        ],
    ]);
    ?>



    <?= CustomGridView::widget([
		'create'=>true,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],
            'id',
            'name',
            ['attribute'=>'username','value'=>function($model)

            {
                $result = \app\models\Deal::find()->where(['hotel_id' => $model->id])->asArray()->all();
               return count($result);


            },'label'=>'No. of Deals'],
/*            'email:email',
            'mobile',
            ['attribute'=>'city_id','value'=>function($model){return $model->city->title;},'filter'=>ArrayHelper::map(City::find()->where(['status'=>1,'trashed'=>0])->orderBy('title')->asArray()->all(),'id','title')],*/

            ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>Yii::t('app','Actions'),
                'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
                'contentOptions'=>['class'=>'noprint text-right'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-folder"></i>', ['view','id'=>$model->id], [
                            'title' => Yii::t('app', 'View'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-info btn-xs',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', ['update','id'=>$model->id], [
                            'title' => Yii::t('app', 'Update'),
                            'data-toggle'=>'tooltip',
                            'class'=>'btn btn-success btn-xs',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-remove"></i>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'data-toggle'=>'tooltip',
                            'data-method'=>'post',
                            'data-confirm'=>Yii::t('app','Are you sure you want to delete this?'),
                            'class'=>'btn btn-danger btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
