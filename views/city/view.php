<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\City */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-view box">
    <div class="box-header with-border">
        <h3></h3>
        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Update']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Delete', 'data'=>['method' => 'post', 'confirm' => Yii::t('app', 'Are you sure you want to delete this?'),]]) ?>
            <?= Html::a('<i class="fa fa-times"></i>', ['index'], ['class' => 'btn btn-box-tool', 'data-toggle'=>'tooltip', 'title'=>'Back']) ?>
        </div>
    </div>
    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
	            'title',
            ['format'=>'html','attribute'=>'status','value'=>function($model){return $model->txtStatus;},'filter'=>Yii::$app->params['statusArr']],
            ],
        ]) ?>
    </div>
</div>
